#include "VoxelLib.h"
#define GET_MAP(x,z) mapHash.get()[((x)%dim + dim) % dim + (((z)%dim + dim) % dim)*dim]
namespace Voxel
{
	HeightMapHash::HeightMapHash()
	{
	}
	HeightMapHash::HeightMapHash(int _dim) : dim(_dim)
	{
		mapHash = shared_ptr<HeightMap>(new HeightMap[_dim*_dim], Deleter::arrayDelete<HeightMap>);
	}
	HeightMapHash::~HeightMapHash()
	{
	}
	void HeightMapHash::GenMap(Vector3i pos)
	{
		HeightMap map = GET_MAP(pos.x,pos.z);
		if (map.built && map.gridPos == pos)
			return;
		//now we can assume we have a new map.
		GET_MAP(pos.x,pos.z) = HeightMap(pos);
		GET_MAP(pos.x, pos.z).Init();
	}
	HeightMap& HeightMapHash::operator[](Vector3i pos)
	{
		HeightMap& map = GET_MAP(pos.x, pos.z);
		return map;
	}
}
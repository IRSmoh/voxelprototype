#pragma once
#include <assert.h>
namespace MemManagement
{
	template<typename T>
	//this is the same size as if you were calling the function normally. aka amount*sizeof(T)
	//uses assert() to force catching of bugs
	T* _realloc(T* ptr, unsigned long long size)
	{
		//this is a terrible name for it, but. it gives the exact error in the abort message... so....
		void* reallocFailed = realloc(ptr, size);
		assert(reallocFailed)
		ptr = reallocFailed;
		return ptr;
	}
}
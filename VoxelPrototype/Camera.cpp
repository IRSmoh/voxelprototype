#include "Camera.h"
#include "Keys.h"
GLuint Camera::g_cameraMatricesUBO;
GLuint Camera::cameraUniformBlockIndex;
GLuint Camera::g_iCameraMatricesBindingIndex;
Camera::Camera()
{
	perspective = Matrix4x4::makePerspective(60, 1, 1, 1000);
	transform = shared_ptr<Transform>(new Transform(Vector3f(0, 0, 0), Vector3f(1, 1, 1), Vector3f(0,0,0)));
}
Camera::Camera(float fov, float aspect, float nearPlane, float farPlane)
{
	perspective = Matrix4x4::makePerspective(fov, aspect, nearPlane, farPlane);
	transform = shared_ptr<Transform>(new Transform(Vector3f(0, 0, 0), Vector3f(1, 1, 1), Vector3f(0, 0, 0)));
}
Camera::~Camera()
{
}
void Camera::setView()
{
	//the layout in the shader is...
	//layout(std140) uniform GlobalMatrices
	//{
	//	mat4 perspective;
	//	mat4 view;
	//	mat4 rotations;
	//};
	transform->UpdateWorldMatrix();
	glBindBuffer(GL_UNIFORM_BUFFER, g_cameraMatricesUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(Matrix4x4), &perspective[0]);
	cameraFlip();
	glBufferSubData(GL_UNIFORM_BUFFER, sizeof(Matrix4x4), sizeof(Matrix4x4), &view[0]);
	glBufferSubData(GL_UNIFORM_BUFFER, sizeof(Matrix4x4)* 2, sizeof(Matrix4x4), &rotMatrix[0]);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}
void Camera::cameraFlip()
{
	//the cameras view is the inverse of it's world matrix, invert it.
	view = transform->worldMatrix.Inverse();

	//and due to a bug with scaling the view matrix (the camera appears to orbit an imaginary point that doesn't exist..)
	//throw out the scale
	view = view*Matrix4x4(view.ExtractInverseScale());
	//copy the view into the rotation matrix so we can have a pure rotation matrix (e.g. for skyboxes and similar constructs)
	rotMatrix = view;
	//zero out the position
	rotMatrix.m14 = rotMatrix.m24 = rotMatrix.m34 = 0;
	//tada, your matrix is now a pure rotation matrix again :D
}

void Camera::initCameraData(GLint shader)
{
	cameraUniformBlockIndex = glGetUniformBlockIndex(shader, "GlobalMatrices");
	GLuint trash = glGetUniformLocation(shader, "mM");
	glGenBuffers(1, &g_cameraMatricesUBO);
	glBindBuffer(GL_UNIFORM_BUFFER, g_cameraMatricesUBO);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(Matrix4x4) * 3, NULL, GL_STREAM_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	//bind the locations together
	glUniformBlockBinding(shader, cameraUniformBlockIndex, g_iCameraMatricesBindingIndex);
	glBindBufferRange(GL_UNIFORM_BUFFER, g_iCameraMatricesBindingIndex, g_cameraMatricesUBO, 0, sizeof(Matrix4x4) * 3);
}
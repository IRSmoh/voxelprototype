#include "Keys.h"



Keys::keyBools Keys::keys[] = { 0 };
Keys::keyBools Keys::mouseKeys[] = { 0 };
double Keys::scrollState[] = { 0 };
double Keys::scrollEvent[] = { 0 };

double Keys::oldMouseX = 0;
double Keys::oldMouseY = 0;
double Keys::deltaX = 0;
double Keys::deltaY = 0;
double Keys::oldDeltaX = 0;
double Keys::oldDeltaY = 0;
float Keys::w = 0;
float Keys::h = 0;

bool Keys::mouseUpdate = false;

void Keys::UpdateKeys(GLFWwindow* window)
{
	for (int key = 0; key < keyCount; key++)
	{
		SetKey(key, glfwGetKey(window, key));
	}
	for (int key = 0; key < mouseCount; key++)
	{
		SetMouseButton(key, glfwGetMouseButton(window, key));
	}
	for (int key = 0; key < scrollCount; key++)
	{
		SetScroll(key);
	}
	MousePoll();
}

void Keys::Init(int width, int height)
{
	w = (float)width;
	h = (float)height;
	oldMouseX = w / 2.0f;
	oldMouseY = h / 2.0f;
}

void Keys::SetKey(int key, int action)
{
	if (key >= 0 && key < keyCount)
	{
		//determine if the key was just pressed this frame
		if (!keys[key].pressed && action == GLFW_PRESS)
		{
			keys[key].justPressed = true;
			keys[key].toggle = !keys[key].toggle;
		}
		else
			keys[key].justPressed = false;
		//determine if the key was just released this frame
		if (keys[key].pressed && action == GLFW_RELEASE)
			keys[key].justReleased = true;
		else
			keys[key].justReleased = false;
		//set the key's current status
		keys[key].pressed = action !=0;
	}
}
bool Keys::GetKey(int key)
{
	if (key >= 0 && key < keyCount)
	{
		return keys[key].pressed;
	}
	return false;
}
bool Keys::KeyToggled(int key)
{
	if (key >= 0 && key < keyCount)
	{
		return keys[key].toggle;
	}
	return false;
}
bool Keys::KeyDown(int key)
{
	if (key >= 0 && key < keyCount)
	{
		return keys[key].justPressed;
	}
	return false;
}
bool Keys::KeyUp(int key)
{
	if (key >= 0 && key < keyCount)
	{
		return keys[key].justReleased;
	}
	return false;
}

void Keys::MousePoll()
{
	if (deltaX != 0)
	if (oldDeltaX == deltaX)
	{
		deltaX = 0;
	}
	if (deltaY != 0)
	if (oldDeltaY == deltaY)
	{
		deltaY = 0;
	}
	oldDeltaX = deltaX;
	oldDeltaY = deltaY;
}
void Keys::MouseMovementCallB(GLFWwindow* window, double x, double y)
{
	if (!KeyToggled(KEY_F))
	{
		mouseUpdate = true;

		deltaX = (x - oldMouseX) / w / 2.0;
		deltaY = (y - oldMouseY) / h / 2.0;
		//__________________________________________
		//											|
		//		send message to input component		|
		//__________________________________________|

		//SpawnVariant deltaXdata;
		//deltaXdata.m_type = SpawnVariant::Type::TYPE_FLOAT;
		//deltaXdata.m_asFloat = deltaX;

		//SpawnVariant deltaYdata;
		//deltaYdata.m_type = SpawnVariant::Type::TYPE_FLOAT;
		//deltaYdata.m_asFloat = deltaY;

		//SpawnEvent e;
		//e.m_type = EventType::EVENT_TYPE_INPUT_RECEIVED;
		//e.m_numArgs = 2;
		//e.m_aArgs[0] = deltaXdata;
		//e.m_aArgs[1] = deltaYdata;

		//SPAWN_GET_SYSTEM(SpawnSys_Input)->inputQueue.EnQueue(e);
		
		oldMouseX = x;
		oldMouseY = y;
		if (oldMouseX > w / 2 + deadzone || oldMouseX < w / 2 - deadzone)
		{
			glfwSetCursorPos(window, w / 2.0f, oldMouseY);
			oldMouseX = w / 2;
		}
		if (oldMouseY > h / 2 + deadzone || oldMouseY < h / 2 - deadzone)
		{
			glfwSetCursorPos(window, oldMouseX, h / 2.0f);
			oldMouseY = h / 2;
		}
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	}
	else
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
}

void Keys::KeyboardCallB(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

void Keys::SetMouseButton(int key, int action)
{
	if (key >= 0 && key < mouseCount)
	{
		//determine if the key was just pressed this frame
		if (!mouseKeys[key].pressed && action == GLFW_PRESS)
		{
			mouseKeys[key].justPressed = true;
			mouseKeys[key].toggle = !mouseKeys[key].toggle;
		}
		else
			mouseKeys[key].justPressed = false;
		//determine if the key was just released this frame
		if (mouseKeys[key].pressed && action == GLFW_RELEASE)
			mouseKeys[key].justReleased = true;
		else
			mouseKeys[key].justReleased = false;
		//set the key's current status
		mouseKeys[key].pressed = action !=0;
	}
}
bool Keys::GetMouseButton(int key)
{
	if (key >= 0 && key < mouseCount)
	{
		return mouseKeys[key].pressed;
	}
	return false;
}
bool Keys::MouseButtonToggled(int key)
{
	if (key >= 0 && key < mouseCount)
	{
		return mouseKeys[key].toggle;
	}
	return false;
}
bool Keys::MouseButtonDown(int key)
{
	if (key >= 0 && key < mouseCount)
	{
		return mouseKeys[key].justPressed;
	}
	return false;
}
bool Keys::MouseButtonUp(int key)
{
	if (key >= 0 && key < mouseCount)
	{
		return mouseKeys[key].justReleased;
	}
	return false;
}
double Keys::ScrollAxis(int axis)
{
	if (axis >= 0 && axis < scrollCount)
	{
		return scrollState[axis];
	}
	return 0;
}
void Keys::SetScrollEvent(GLFWwindow* window, double x, double y)
{
	scrollEvent[SCROLL_AXIS_HORIZONTAL] = x;
	scrollEvent[SCROLL_AXIS_VERTICAL] = y;
}
void Keys::SetScroll(int key)
{
	if (key >= 0 && key < scrollCount)
	{
		if (scrollEvent[key] != scrollState[key])
		{
			scrollState[key] = scrollEvent[key];
			scrollEvent[key] = 0;
		}
		else
		{
			//sometimes we can skip a callback, so if the two scroll states happen to be the same, forcibly set them to zero.
			//this is to fix an issue when you scroll rapidly causing the state to stick.
			scrollEvent[key] = 0;
		}
	}
}
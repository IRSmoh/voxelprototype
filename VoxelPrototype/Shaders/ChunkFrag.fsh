#version 400


in vec4 color;
out vec4 fColor;

in vec3 texCoord;
uniform sampler2DArray text;

void main () 
{
	fColor = texture(text, texCoord);
	fColor *= color;
	if(fColor.a <= 0.001)
	{
		discard;
	}
}
#version 400

// Combined our old stuff with the shader from Angel book

in vec4 vPosition;
in vec4 vNormal;
uniform mat4 model;

in vec3 vTexCoord;
out vec3 texCoord;

in vec4 vColor;
out vec4 color;

layout(std140) uniform GlobalMatrices
{
	mat4 perspective;
	mat4 view;
	mat4 rotations;
};


void main () 
{
	gl_Position = perspective*view*model*vPosition;
	texCoord = vTexCoord;
	color = vColor;
}
#version 400

in vec4 vPosition;
uniform mat4 model;
out vec3 texCoord;

layout(std140) uniform GlobalMatrices
{
	mat4 perspective;
	mat4 view;
	mat4 rotations;
};


void main () 
{
	//position the cube
	gl_Position = perspective*rotations*model*vPosition;

	//get out weird ass text coords for the cubemap, apparently it wants a vec3 of our world location?
	texCoord = vPosition.xyz;
}
#version 400


out vec4 vFragColor;

in vec3 texCoord;
uniform samplerCube cubeMap;

void main () 
{
	vFragColor = texture(cubeMap, texCoord);
}
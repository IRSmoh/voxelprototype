#pragma once
#include "Keys.h"
#include "Camera.h"
#include "GameObject.h"
#include <memory>
#include "VoxelMap.h"
#include "VoxelLib.h"
class Player
{
private:
	float movespeed = 10.0f;
	float speed;
	float speedMult = 10.0f;
	float rotSpeed;
	float baseRotSpeed = 4.5f;
	float deltaTime;
	Vector3f pos;
	int currBlock;
public:
	
	shared_ptr<Transform> transform;	//Temp stuff
	shared_ptr<Model> model;			//Temp stuff



	bool inverted;
	//keys will be cleaned up on game close.
	shared_ptr<Camera> camera;
	Voxel::Map* map;
	Player();
	Player(Voxel::Map* map, shared_ptr<Camera> camera, Vector3f position);
	~Player();

	virtual void Update();
	//void Rotate(Quaternion& rotations);
	void Rotate(float deltaX, float deltaY);
};


#pragma once
#include "VoxelLib.h"
namespace Voxel
{
	//make this have a normal cubic hash table for immediate lookup
	//then have a 'lazy' cache of the 1 distance surrounding the hash for lazy lookup. 
	//e.g. moving into a new region would allow us to not have to read from disk again necessarily,
	//this would also allow us to store the extra regions to disk and not have to check them for refreshes.
	//assuming this runs well, this will be a maximally compressed chunk of backup memory that will prevent a lot of IO overhead.
	class RegionManager
	{
	private:
		std::vector<std::shared_ptr<Region>> active_regions;	//our regions that are frequently accessed/active chunks are still part of
		std::vector<std::shared_ptr<Region>> sleeping_regions;	//for regions that recently went out of bounds, needing to be serialized
		int regionDim;	//this is the dimensionality of our vector, aka ^3
	public:
		static float Region_Sleep_Delay;	//if the region has 'aged' to this point, destroy it.
		float lastStoreTime;		//simple counter for using with stash delay.
		static float Stash_Delay;	//indicates how often we should try and store region edits.
		RegionManager();
		RegionManager(int loadedChunkDim);
		~RegionManager();
		void Update(float deltaTime);
		void loadRegionsFromDisk();
		void ResizeLoadedChunks(int loadedChunkDim);
		void StoreChunk(Chunk& chunk);
		//this returns the block array for the chunk. Specify the desired chunk to load, e.g. (0,-1,10) NOTE: confirm we found a chunk, check if ptr is != nullptr
		std::shared_ptr<BlockID> GetChunkData(Vector3i chunk_world_pos);
	};
}
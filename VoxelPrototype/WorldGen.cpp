#include "VoxelLib.h"
namespace Voxel
{
	OSN::Noise<3> WorldGen::noise3D = OSN::Noise<3>();
	OSN::Noise<2> WorldGen::noise2D = OSN::Noise<2>();

#define spawnBlock 0.5555f
#define heightMult 100.0f
#define flatness 20.0f
#define caveSize 40.0f
#define featureSize 400.0f
#define heightOffset 6*Chunk::Chunk_Side
#define octaves 4
#define basePow 2.0f
#define divPow 1.567f
	BlockID WorldGen::genBlock(Vector3i pos, float height)
	{
		if (pos.y < (height)*heightMult + heightOffset + 4)
		{
			float caveval = noise3D.eval(pos.x / (caveSize + flatness), (pos.y + heightOffset) / caveSize, pos.z / (caveSize + flatness));
			if (pos.y >(height)*heightMult + heightOffset + 3)
			{
				return caveval > spawnBlock ? 0 : 3;
			}
			if (pos.y > height*heightMult + heightOffset)
			{
				return caveval > spawnBlock ? 0 : 4;
			}
			if (caveval < spawnBlock)
			{
				if (pos.y < (height + 1)*Chunk::Chunk_Side)
					return 2;
				else
					return 1;
			}
		}
		return 0;
	}
	float WorldGen::genHeight(int x, int z)
	{
		float mult, div;
		float val = 0;
		for (int i = 1; i <= octaves; i++)
		{
			//pow works better with float than ints...?
			mult = pow(basePow, i);
			div = pow(divPow, i);
			val += noise2D.eval(x / featureSize*mult, z / featureSize*mult) / div;
		}
		return val;
	}
}
#pragma once
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "Transform.h"
//before we forget about it, the camera must have an entirely negated matrix4x4, this includes position being inverted, and rotation being inverted also.
//specifies the view/perspective matrix in the shaders, only requires calling setView() once per frame, not once per object.
class Camera 
{
private:
	Matrix4x4 view;
	Matrix4x4 rotMatrix;
	void cameraFlip();
	static GLuint cameraUniformBlockIndex;
	//perspective then view for the order of the 2 matrices
	static GLuint g_cameraMatricesUBO;
	static GLuint g_iCameraMatricesBindingIndex;
public:
	shared_ptr<Transform> transform;	//temp stuff 

	Matrix4x4 perspective;
	Camera();
	Camera(float fov, float aspect, float nearPlane, float farPlane);
	~Camera();
	void setView();

	static void initCameraData(GLint shader);
};


#pragma once
#include "GameObject.h"
#include "CubeMap.h"

class Transform;
class Model;
//renders a cubemap to the screen, supply it the cameras orientation only, no positional data.
//the size of the skybox should be strictly larger than the near plane
class SkyBox
{
protected:
	CubeMap cubemap;
	void genVerts();
public:

	shared_ptr<Transform> transform;			//Temp stuff
	shared_ptr<Model> model;					//Temp stuff
	shared_ptr<Shader> shader;					//Temp stuff


	SkyBox();
	SkyBox(shared_ptr<Shader> shader);
	void Init();
	//incase you want to treat this as a normal object, although it ignores the objects translation/rotation/everything
	virtual ~SkyBox();
	virtual void Render();
	virtual void SetCubeMap(CubeMap& map);
};


#include "GlobalGraphicsData.h"

#pragma region CameraData
GLuint GlobalGraphicsData::g_cameraMatricesUBO;
GLuint GlobalGraphicsData::cameraUniformBlockIndex;
GLuint GlobalGraphicsData::g_iCameraMatricesBindingIndex;

void GlobalGraphicsData::initCameraData(Shader* shader)
{
	cameraUniformBlockIndex = glGetUniformBlockIndex(shader->programID, "GlobalMatrices");
	GLuint trash = glGetUniformLocation(shader->programID, "mM");
	glGenBuffers(1, &g_cameraMatricesUBO);
	glBindBuffer(GL_UNIFORM_BUFFER, g_cameraMatricesUBO);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(Matrix4x4)*3, NULL, GL_STATIC_DRAW); //Switched from stream draw. Static is meant to be used often, stream is not
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	//bind the locations together
	glUniformBlockBinding(shader->programID, cameraUniformBlockIndex, g_iCameraMatricesBindingIndex);
	glBindBufferRange(GL_UNIFORM_BUFFER, g_iCameraMatricesBindingIndex, g_cameraMatricesUBO, 0, sizeof(Matrix4x4)*3);
}
#pragma endregion CameraData

#pragma region VoxelTextures
Texture2DArray GlobalGraphicsData::voxelTexts;

void GlobalGraphicsData::InitVoxelTexts(Shader* shader)
{
	const char* stone = "Textures\\sunken.png";
	const char* quartz = "Textures\\quartz.png";
	const char* dirtTop = "Textures\\dirt_top.png";
	const char* dirtSide = "Textures\\dirt_side.png";
	const char* dirtBottom = "Textures\\dirt.png";
	voxelTexts = Texture2DArray(GL_RGBA, 5);
	//unsigned int error = voxelTexts.GetError();
	voxelTexts.AddTexture(stone);
	voxelTexts.AddTexture(quartz);
	voxelTexts.AddTexture(dirtTop);
	voxelTexts.AddTexture(dirtSide);
	voxelTexts.AddTexture(dirtBottom);
	voxelTexts.FinalizeTexture(shader->programID);
}
#pragma endregion VoxelTextures
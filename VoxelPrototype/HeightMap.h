#pragma once
#include "VoxelLib.h"
namespace Voxel
{
	class HeightMap
	{
	private:
		shared_ptr<float> height_map;
	public:
		static int Map_Side;
		static int Map_Size;
		bool built;
		Vector3i gridPos;
		HeightMap();
		HeightMap(Vector3i gridPos);
		~HeightMap();
		void Init();
		//access layout is [x + z*dim]
		float& operator[](Vector3i pos);
	};
}
#include "VoxelLib.h"
namespace Voxel
{
	Map::Map()
	{
	}
	Map::Map(std::shared_ptr<Shader> shader, int mapDim)
	{
		transform = std::shared_ptr<Transform>(new Transform(Vector3f(0, 0, 0), Vector3f(1, 1, 1), Vector3f(0, 0, 0)));
		manager = std::shared_ptr<ChunkManager>(new ChunkManager(mapDim, shader, transform.get()));
	}
	Map::~Map()
	{
	}
	void Map::Update()
	{
		manager->Update();
	}
	void Map::PlaceBlock(BlockID block, Vector3i pos)
	{
		Chunk* chunk = manager->GetOwningChunk(pos);
		if (chunk)
		{
			chunk->PlaceBlock(block, pos.preserveOffset((int)Chunk::Chunk_Side));
		}
	}
	void Map::GetBlock(Vector3i pos, bool& found, BlockID& block)
	{
		Chunk* chunk = manager->GetOwningChunk(pos);
		if (chunk)
		{
			found = true;
			block = (*chunk)[pos & Chunk::Chunk_BitMask];
			return;
		}
		found = false;
		block = 0;
	}
	void Map::Render()
	{
		if (Keys::KeyToggled(KEY_T))
		{
			manager->wireframe = true;
		}
		else
		{
			manager->wireframe = false;
		}
		manager->RenderChunks();
	}
}
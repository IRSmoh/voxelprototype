#pragma once
namespace Deleter
{
	template <class T>
	void Free(T* ptr)
	{
		free(ptr);
	}
	template <class T>
	void arrayDelete(T* ptr)
	{
		delete[] ptr;
	}
}
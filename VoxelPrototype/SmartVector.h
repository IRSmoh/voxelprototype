#pragma once
#include <vector>
#include <stdint.h>
using namespace std;
namespace SmartVector
{
	template<typename T>
	void smartDelete(vector<T>& vec, unsigned int pos)
	{
		swap(vec[pos], vec[vec.size() - 1]);
		vec.pop_back();
	}
}
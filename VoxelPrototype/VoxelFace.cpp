#include "VoxelFace.h"


VoxelFace::VoxelFace()
{
	transparent = true;
	type = -1;
	side = -1;
}

VoxelFace::VoxelFace(bool transparent, int type, int side)
{
	this->transparent = transparent;
	this->type = type;
	this->side = side;
}
VoxelFace::~VoxelFace()
{
}

#pragma once
#include "Matrix4x4.h"
#include "Vector3f.h"
#include "Quaternion.h"
#include <memory>
#include <string>
#include <vector>
using namespace std;
class GameObject;
//stores the position/rotation/scale of an object, and if supplied a parent transform calculates its relative world position based on the mat4 of its parent(s)
class Transform
{
public:
	Matrix4x4 worldMatrix;      //if the parent/this hasn't moved, why remake it?
	Matrix4x4 localMatrix;		//the local matrix, so we can cut back on some math.
	Vector3f position;			//should be pulled from matrix
	Vector3f scale;				//the scale of the object(s)
	Vector3f eulerAngles;
	Transform* parent;			//the transform that we are attatched to, if null then it's the root, and thus 1:1 with the world.
	static bool toggleBool;		//switch to indicate a new frame. switch via toggleBool = !toggleBool;
	bool regenLocalMatrix : 1;		//this gets set any time the rotation/position/scale of an object is changed.
	bool regenWorldMatrix : 1;		//this gets changed any time this objects world matrix should change (now to figure out how to do that, without it being buggy)
	bool stateBool : 1;				//determine if this is a different frame or not
	bool regenChildren : 1;			//determine if children need to be regened


	Transform(Vector3f position, Vector3f scale, Vector3f eulerAngles);
	void UpdateLocalMatrix();
	//returns a matrix useable for rendering. Recursive warning!
	void UpdateWorldMatrix();
	//moves by dist*rotation e.g. cares about orientation of transform
	void Move(Vector3f dist);
	Vector3f localPosition();
	Vector3f worldPosition();
};
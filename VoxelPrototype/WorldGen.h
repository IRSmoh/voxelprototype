#pragma once
#include "VoxelLib.h"
namespace Voxel
{
	//this will supply 3d world generation functions
	class WorldGen
	{
	public:
		static OSN::Noise<3> noise3D;
		static OSN::Noise<2> noise2D;
		static BlockID genBlock(Vector3i pos, float height);
		static float genHeight(int x, int z);
	};
}
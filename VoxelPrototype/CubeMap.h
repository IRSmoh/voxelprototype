#pragma once
#include "Texture.h"
#include <string>
#include <iostream>
//supported formats:
//6 separate files
//1 file (vertical or horizontal cross)
//order respecifier on 1 file type
class CubeMap : public Texture
{
protected:
	//because typing vector<unsigned char> is hard.
	typedef vector<unsigned char> pixelData;
	//6 sides
	pixelData left;
	pixelData right;
	pixelData top;
	pixelData bottom;
	pixelData front;
	pixelData back;
	//split the larger cross format into sub formats and store the data into the appropriate left/right/etc
	void ParseFile();
	void copySubPic(pixelData& image, int imageWidth, int minHeight, int maxHeight, int minWidth, int maxWidth, pixelData& result, int channelCount);
public:
	GLuint cubemapID;
	CubeMap();
	CubeMap(string& topFile, string& bottomFile, string& leftFile, string& rightFile, string& backFile, string& frontFile, int channelType);
	//e.g. GL_RGBA or GL_RGB
	CubeMap(string& file, int channelType);
	virtual ~CubeMap();

	virtual void PushTexture(GLint shaderID) override;
};


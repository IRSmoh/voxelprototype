#include "Texture2DArray.h"


Texture2DArray::Texture2DArray() : Texture()
{
	layerCount = 1;
	channelType = GL_RGBA;
	textureType = GL_TEXTURE_2D_ARRAY;
}
Texture2DArray::Texture2DArray(int channelType, int numLayers) : Texture()
{
	layerCount = 1;
	this->channelType = channelType;
	this->numLayers = numLayers;
	textureType = GL_TEXTURE_2D_ARRAY;
}
Texture2DArray::~Texture2DArray()
{
}

void Texture2DArray::AddTexture(const char* filename)
{
	lodepng::decode(image, width, height, filename);

	//check to make sure we are on the first texture, if not we've allocated a texture buffer
	if (textureID == -1)
	{
		glGenTextures(1, &textureID);
		glBindTexture(GL_TEXTURE_2D_ARRAY, textureID);
		glEnable(textureID);
		/*
		https://www.opengl.org/sdk/docs/man/html/glTexImage3D.xhtml
		void glTexImage3D(	GLenum target,
		GLint level,
		GLint internalFormat,
		GLsizei width,
		GLsizei height,
		GLsizei depth,
		GLint border,
		GLenum format,
		GLenum type,
		const GLvoid * data);

		note: can pass NULL to specify later data submissions, similar to glTexStorage3D except I couldn't get that working nicely.
		*/
		glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, channelType, width, height, numLayers, 0, channelType, GL_UNSIGNED_BYTE, NULL);
	}
	/*
	https://www.opengl.org/sdk/docs/man3/xhtml/glTexSubImage3D.xml

	because shockingly, I can read function parameters...

	void glTexSubImage3D(GLenum target,
 	GLint level,
 	GLint xoffset,
 	GLint yoffset,
 	GLint zoffset,
 	GLsizei width,
 	GLsizei height,
 	GLsizei depth,
 	GLenum format,
 	GLenum type,
 	const GLvoid * data);
	*/
	glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, width, height, layerCount, channelType, GL_UNSIGNED_BYTE, &image[0]);
	++layerCount;
}
void Texture2DArray::FinalizeTexture(GLint shaderID)
{
	//gen mip maps, specify filtering etc
	glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//not mipmapped
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//mipmapped.
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	//eventually this will be removed, and replaced with an index, but realistically only one shader will want a texture2d array
}

#pragma once
#include "Texture.h"
//lazy mans texture atlas.
//you must specify the number of textures you will be supplying at construction, the max is ~2048
class Texture2DArray : public Texture
{
public:
	//error if loaded images do not match.
	static const int INVALID_SIZE = 1;
	static const int mipLayers = 8;
	int layerCount;
	int numLayers;
	Texture2DArray();
	Texture2DArray(int channelType, int numLayers);
	virtual ~Texture2DArray();
	//call finalizeTexture afterwards so we can gen mip maps, and other similar things, do not process other textures inbetween adding to this, and finalizing.
	virtual void AddTexture(const char* filename);
	virtual void FinalizeTexture(GLint shaderID);
};


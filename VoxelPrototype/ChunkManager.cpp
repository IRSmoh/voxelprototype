#include "VoxelLib.h"
namespace Voxel
{
#define CHUNK_MIN_RADIUS 0
	ChunkManager::ChunkManager()
	{
		chunkDim = 0;
		world_transform = NULL;
		meshing = false;
		exitThreads = false;
		notified = false;
		wireframe = false;
		region = new Region(Vector3i(0, 0, 0));
	}
	ChunkManager::ChunkManager(int mapSide, std::shared_ptr<Shader> shader, Transform* world_transform)
	{
		chunkDim = mapSide;
		Chunks = ChunkHash(mapSide);
		regionManager = RegionManager(mapSide);
		heightMap = HeightMapHash(mapSide);
		this->world_transform = world_transform;
		this->shader = shader;
		std::thread builderMesher = std::thread(Build_Mesh, this);
		builderMesher.detach();
		meshing = false;
		exitThreads = false;
		notified = false;
		wireframe = false;
		region = new Region(Vector3i(0, 0, 0));
		bool stats = region->loadFromDisk();
		if (!stats)
		{
			delete(region);
			region = new Region(Vector3i(0, 0, 0));
		}
	}
	ChunkManager::~ChunkManager()
	{
		exitThreads = true;
		cv_main.notify_all();
		region->storeToDisk();
		delete(region);
	}
	void ChunkManager::StoreChunk(Chunk& chunk)
	{
		if (chunk.isStale)
		{
			chunk.isStale = false;
			regionManager.StoreChunk(chunk);
		}
	}
	void ChunkManager::LoadChunk(Vector3i chunkPos)
	{
		//determine if the chunk exists in the region, if it does, decompress it and pass it off to the chunk to use.
		auto compressedData = regionManager.GetChunkData(chunkPos);
		Chunks[chunkPos] = Chunk(shader, chunkPos, this);
		Chunks[chunkPos].transform->parent = world_transform;
		if (compressedData != nullptr)
		{
			Chunks[chunkPos].AssignBlocks(compressedData);
		}
		else
		{
			Chunks[chunkPos].DirtyAllNeighbors();
			buildChunksList.push_back(&Chunks[chunkPos]);
		}
	}
	void ChunkManager::UpdateRenderable()
	{
		//determine what chunks need to be rendered.
		//hint: this will only come from the hashtable of chunks, and it will be based on the orientation of the camera in 3d space.
	}
	void ChunkManager::LoadNearbyChunks(Vector3i pos)
	{
		//note: this needs to be run in a not main loop thread to prevent delays when trying to load chunks/decompress chunks.
		if (!meshing)
		{
			pos >>= Chunk::Chunk_BitShift;
			//ugly ugly ugly shit
			Vector3i checkPos[] = { Vector3i::Up(), Vector3i::Forward(), Vector3i::Right(), Vector3i::Down(), Vector3i::Back(), Vector3i::Left() };
			bool regen = false;
			Vector3i tmp;
			int chunkLoadRadius = (chunkDim / 2 - (chunkDim % 2 ? 1 : 0));
			for (int i = 0; i < 6; i++)
			{
				tmp = pos + checkPos[i] * (chunkLoadRadius - CHUNK_MIN_RADIUS);
				regen |= Chunks[tmp].arrayPos != tmp;
			}
			//end ugly
			if (regen)
			{
				//figure out what chunks need to be loaded
				//either put those in a list or directly create new chunks
				//let the rest of the manager run, and things should be loaded.
				//- (chunkDim % 2 ? 1 : 0) -> determine if we need to offset the center of the loaded chunks or not,
				for (int x = pos.x - chunkDim / 2 - (chunkDim % 2 ? 1 : 0); x < pos.x + chunkDim / 2; x++)
				{
					for (int y = pos.y - chunkDim / 2 - (chunkDim % 2 ? 1 : 0); y < pos.y + chunkDim / 2; y++)
					{
						for (int z = pos.z - chunkDim / 2 - (chunkDim % 2 ? 1 : 0); z < pos.z + chunkDim / 2; z++)
						{
							ForceLoaded(Vector3i(x, y, z));
						}
					}
				}
			}
		}
	}
	void ChunkManager::ForceLoaded(Vector3i pos)
	{
		if (Chunks[pos].isNew || Chunks[pos].arrayPos != pos)
		{
			LoadChunk(pos);
		}
	}
	Chunk* ChunkManager::GetOwningChunk(Vector3i pos)
	{
		Vector3i fetchPos = pos >> Chunk::Chunk_BitShift;
		Chunk* chunk = &Chunks[fetchPos];
		if (chunk->arrayPos == fetchPos)
		{
			return chunk;
		}
		return NULL;
	}
	void ChunkManager::Update()
	{
		if (!buildChunksList.empty() || !dirtyChunksList.empty())
		{
			//signal the condition variable that the thread has work to do.
			meshing = true;
			if (!notified)
			{
				notified = true;
				cv_main.notify_one();
			}
		}
		regionManager.Update(Timer::deltaSec());
	}
	void ChunkManager::Build_Mesh(ChunkManager* manager)
	{
		int offset, threadLength;
		//determine our maximum number of effective concurrent threads
		//e.g. get the core count of the cpu, if hyperthreading is enabled this is 2* the core count.
		//additionally, we need dedicated threads for the building/meshing, so we can't share threads between operations, thus the *2
		manager->threadCount = std::thread::hardware_concurrency();
		int threadCount = manager->threadCount;
		if (threadCount == 0)
		{
			threadCount = 8;
		}
		//half the threads will go to building, the other half will go to meshing.
		shared_ptr<thread> threads = shared_ptr<thread>(new thread[threadCount * 2], Deleter::arrayDelete<thread>);

		//we need this in order to pass thread data around, e.g. the beginning and end of the data this thread needs to parse.
		std::vector<int> threadWork_Build = std::vector<int>(threadCount * 2, 0);
		std::vector<int> threadWork_Mesh = std::vector<int>(threadCount * 2, 0);
		//we are using the thread counter as a way of synchronizing the many builder/mesher threads together.
		//since we have to wait for all the builders to finish before we can mesh we need a way of doing so.
		//if the builders have finished all of them will have incremented the counter, the order is irrelevant, so long as all do.
		//once that happens we can signal for this thread to continue working, and set the counter back to zero, repeat forever (or until program exit)
		manager->thread_counter = 0;


		//now that we have our threads, and they are auto managed, we need to create a way of syncing between all the child threads.
		//this unfortunately means we need to use atomic_int and count up to 1/2 the thread count, when we hit that number signal that the worker threads are done and proceed to the next step
		//during that time this thread will be stalled out/waiting
		//once the builders are done move to the meshers etc
		//whenever the worker threads need to do work, store the starting point, and the end point into an array they can all access, then do cv.notify_all() or something similar.
		mutex m;
		std::unique_lock<std::mutex> lk(m);
		//since I don't want to look at the current loop, instead I'll rewrite it.
		//build our threads
		for (int i = 0; i < threadCount; i++)
		{
			threads.get()[i] = thread(BuildChunks, &manager->buildChunksList, &threadWork_Build, manager, i);
			threads.get()[i].detach();
			threads.get()[i + threadCount] = thread(GenMeshes, &manager->dirtyChunksList, &threadWork_Mesh, manager, i);
			threads.get()[i + threadCount].detach();
		}
		while (!manager->exitThreads)
		{
			manager->cv_main.wait(lk);
			if (manager->exitThreads)
			{
				manager->cv_build.notify_all();
				manager->cv_mesh.notify_all();
				return;
			}
			if (!manager->buildChunksList.empty())
			{
				manager->meshing = true;
				//generate the heightmap, sure this could be threaded also, but ffs I'm lazy / this is so tiny of time usually..
				for (size_t i = 0; i < manager->buildChunksList.size(); i++)
				{
					manager->heightMap.GenMap(manager->buildChunksList[i]->arrayPos);
				}
				threadLength = (int)manager->buildChunksList.size() / threadCount;
				offset = (int)manager->buildChunksList.size() % threadCount;
				//generate our portion of the thread data.
				for (int i = 0; i < threadCount; i++)
				{
					threadWork_Build[i*2] = i*threadLength;
					threadWork_Build[i*2 + 1] = threadLength + ((i == threadCount - 1) ? offset : 0);
				}
				//we have finished assigning the chunks for the threads to build, now tell them to 'do the needful'
				manager->cv_build.notify_all();
				manager->cv_main.wait(lk);
				manager->buildChunksList.clear();
			}
			if (!manager->dirtyChunksList.empty())
			{
				manager->meshing = true;
				threadLength = (int)manager->dirtyChunksList.size() / threadCount;
				offset = (int)manager->dirtyChunksList.size() % threadCount;
				//assign the work to do.
				for (int i = 0; i < threadCount; i++)
				{
					threadWork_Mesh[i*2] = i*threadLength;
					threadWork_Mesh[i*2 + 1] = threadLength + ((i == threadCount - 1) ? offset : 0);
				}
				//notify the threads to go do their work.
				manager->cv_mesh.notify_all();
				manager->cv_main.wait(lk);
				manager->dirtyChunksList.clear();
			}
			//the chunks have to be compressed and stored into the region manager anyway, and its not fast enough to compress them in a main thread, so we do it here instead.
			//not to mention the usual race conditions that can occur anyway, so its best to just keep shit here.
			for (int z = 0; z < manager->chunkDim; z++)
			{
				for (int y = 0; y < manager->chunkDim; y++)
				{
					for (int x = 0; x < manager->chunkDim; x++)
					{
						manager->StoreChunk(manager->Chunks[Vector3i(x, y, z)]);
					}
				}
			}
			manager->meshing = false;
			manager->notified = false;
		}
	}
	void ChunkManager::GenMeshes(std::vector<Chunk*>* dirtyChunks, std::vector<int>* work, ChunkManager* manager, int threadID)
	{
		std::mutex m;
		std::unique_lock<std::mutex> lk(m);
		int start, duration;
		while (!manager->exitThreads)
		{
			manager->cv_mesh.wait(lk);
			if (manager->exitThreads)
			{
				return;
			}
			start = (*work)[threadID*2];
			duration = (*work)[threadID*2 + 1];
			for (int i = start; i < start + duration; i++)
			{
				(*dirtyChunks)[i]->BuildMesh();
			}
			++manager->thread_counter;
			if (manager->thread_counter == manager->threadCount)
			{
				manager->thread_counter = 0;
				manager->cv_main.notify_one();
			}
		}
	}
	void ChunkManager::BuildChunks(std::vector<Chunk*>* buildChunks, std::vector<int>* work, ChunkManager* manager, int threadID)
	{
		std::mutex m;
		std::unique_lock<std::mutex> lk(m);
		int start, duration;
		while (!manager->exitThreads)
		{
			manager->cv_build.wait(lk);
			if (manager->exitThreads)
			{
				return;
			}
			start = (*work)[threadID*2];
			duration = (*work)[threadID*2 + 1];
			for (int i = start; i < start + duration; i++)
			{
				(*buildChunks)[i]->BuildChunk();
			}
			++manager->thread_counter;
			if (manager->thread_counter == manager->threadCount)
			{
				manager->thread_counter = 0;
				manager->cv_main.notify_one();
			}
		}
	}
	void ChunkManager::RenderChunks()
	{
		//render the list of chunks, however the hell that's meant to be setup...
		for (int i = 0; i < Chunks.TableSize(); i++)
		{
			Chunks[i].model->wireframe = wireframe;
			Chunks[i].Render();
			//since this will go through all chunks, it makes sense for this to be the section checking.
			//if for some reason another part of the code instead controls looking through all chunks, move this to there.
			if (!meshing)
			{
				if (Chunks[i].isDirty)
				{
					dirtyChunksList.push_back(&Chunks[i]);
				}
			}
		}
	}
}
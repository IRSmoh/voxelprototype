#include "Timer.h"
chrono::time_point<chrono::system_clock, chrono::system_clock::duration> Timer::startTime;
chrono::duration<chrono::system_clock::rep, chrono::system_clock::period> Timer::endTime;
#define MICRO_TO_MILLI 1000.0
#define MICRO_TO_SEC 1000000.0
void Timer::InitTime()
{
	startTime = chrono::high_resolution_clock::now();
	endTime = startTime - startTime;
}
void Timer::ForwardTimer()
{
	endTime = chrono::high_resolution_clock::now() - startTime;
	startTime = chrono::high_resolution_clock::now();
}
double Timer::deltaMicroSec()
{
	return (double)chrono::duration_cast<chrono::microseconds>(endTime).count();
}
double Timer::deltaMilliSec()
{
	return (double)chrono::duration_cast<chrono::microseconds>(endTime).count() / MICRO_TO_MILLI;
}
double Timer::deltaSec()
{
	double val = (double)chrono::duration_cast<chrono::microseconds>(endTime).count() / MICRO_TO_SEC;
	return val;
}
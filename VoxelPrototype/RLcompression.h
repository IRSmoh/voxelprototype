#pragma once
#include <memory>
#include <tuple>
#include "Deleter.h"

template<typename num>
//if the data took more space to compress than length, the pointer/length returned are: nullptr, 0
//else get<0> will return the shared ptr, and get<1> will return the size of the compressed array.
std::tuple<std::shared_ptr<num>, unsigned long long> compress(num* data, unsigned long long length)
{
	unsigned long long curr, offset;
	curr = offset = 0;
	num count;
	count = 0;
	num currData = data[0];
	//if the number is odd, we still need to be able to store the resulting 'over access'
	num* datPtr = (num*)malloc((length + length % 2)*sizeof(num));
	if (datPtr == nullptr)
	{
		return std::tuple<std::shared_ptr<num>, unsigned long long>(nullptr, 0);
	}
	while (curr < length)
	{
		count = 1;
		while (true)//need a loop to chew through numbers anyway.
		{
			if (curr + 1 >= length || currData != data[curr + 1] || static_cast<num>(count + 1) <= 0)
			{
				datPtr[offset] = currData;
				datPtr[offset + 1] = count;
				offset += 2;
				currData = data[++curr];
				break;
			}
			else
			{
				++count;
				++curr;
			}
		}
		//check to make sure we are sucessfully compressing data.
		//this however does cause 'compressed' and not compressed data to display the same.
		//[1,1,2,2,] would be thrown out since the compressed data would be [1,2,2,2] (or whatever the correct order for element/count is) 
		if (offset + 2 >= length)
		{
			return std::tuple<std::shared_ptr<num>, unsigned long long>(nullptr, 0);
		}
	}
	//we have already skipped past the case where we don't have to resize, so we can assume we must resize.
	num* tmp = (num*)realloc(datPtr, offset*sizeof(num));
	if (tmp)
		datPtr = (num*)tmp;
	return std::tuple<std::shared_ptr<num>, unsigned long long>(std::shared_ptr<num>(datPtr, Deleter::Free<num>), offset);
}

template < typename T>
//note: if you are unsure of the decompressedSize specify 0, and the function will scan through the compressed data to calculate the size itself.
//get<0> returns the decompressed array, get<1> returns the size of the array.
std::tuple<std::shared_ptr<T>, unsigned long long> decompress(T* compressedData, unsigned long long compressedSize, unsigned long long decompressedSize)
{
	if (compressedSize == 0 || compressedData == nullptr)
	{
		//you gave me crap, I'll give you crap right back <3
		return std::tuple<std::shared_ptr<T>, unsigned long long>(nullptr, 0);
	}
	T* uncompressedData = nullptr;
	unsigned long long size = decompressedSize;
	if (size == 0)
	{
		//we have to figure out what our size is now.
		for (unsigned long long i = 0; i < compressedSize; i += 2)
		{
			size += compressedData[i + 1];
		}
	}
	uncompressedData = (T*)malloc(size*sizeof(T));
	if (uncompressedData == nullptr)
	{
		//initial alloc failed. why don't you buy more mememory.
		return std::tuple<std::shared_ptr<T>, unsigned long long>(nullptr, 0);
	}

	T data;
	unsigned long long outCount, innerCount, i, j;
	outCount = innerCount = i = j = 0;
	for (i = 0; i < compressedSize; i += 2)
	{
		data = compressedData[i];	//extract the data
		innerCount = compressedData[i + 1];	//extract the count
		//expand the data
		for (j = 0; j < innerCount; j++)
		{
			uncompressedData[outCount + j] = data;
		}
		outCount += innerCount;
	}
	return std::tuple<std::shared_ptr<T>, unsigned long long>(std::shared_ptr<T>(uncompressedData, free), size);
}
#include "VoxelLib.h"
namespace Voxel
{
#define GET_CHUNK(x,y,z) table.get()[((x)%size + size) % size + (((y)%size + size) % size)*size + (((z)%size + size) % size)*size*size];
	ChunkHash::ChunkHash(int size)
	{
		tableSize = size*size*size;
		this->size = size;
		table = std::shared_ptr<Chunk>(new Chunk[tableSize], Deleter::arrayDelete<Chunk>);
	}
	ChunkHash::ChunkHash()
	{
		tableSize = 0;
		this->size = 0;
		table = NULL;
	}
	ChunkHash::~ChunkHash()
	{
	}
	int ChunkHash::DimSize() const
	{
		return size;
	}
	int ChunkHash::TableSize() const
	{
		return tableSize;
	}
	Chunk* ChunkHash::GetChunk(Vector3i chunkArrayPos)
	{
		Chunk* chunk = &GET_CHUNK(chunkArrayPos.x, chunkArrayPos.y, chunkArrayPos.z);
		if (chunk->arrayPos == chunkArrayPos)
		{
			return chunk;
		}
		return NULL;
	}
	Chunk& ChunkHash::operator[](Vector3i& ind)
	{
		return GET_CHUNK(ind.x, ind.y, ind.z);
	}
	Chunk& ChunkHash::operator[](unsigned int ind)
	{
		return table.get()[ind];
	}
}
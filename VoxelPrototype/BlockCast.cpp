#include "VoxelLib.h"
namespace Voxel
{
	void BlockCast::Cast(Vector3f startPos, Vector3f dir, float maxDist, bool& hit, Vector3i& hitPos, Vector3i& lastPos, Map& map, bool ignoreColliders)
	{
		dir.Normalize();
		assert(dir != Vector3f() && "Do not specify a direction of zero for BlockCast, it makes no sense");
		BlockID block;
		//check the case of us being inside a block,
		map.GetBlock(Vector3f::floor(startPos), hit, block);
		if (!ignoreColliders)
		{
			if (MapData::blocks[block].solid && block != Chunk::Air_Block)
			{
				hit = true;
				lastPos = Vector3f::floor(startPos);
				hitPos = Vector3f::floor(startPos);
				return;
			}
			else
			{
				if (block != Chunk::Air_Block)
				{
					hit = true;
					lastPos = Vector3f::floor(startPos);
					hitPos = Vector3f::floor(startPos);
					return;
				}
			}
		}
		//ok, we aren't proceed onto normal logic
		//since I'm not going to write a phd thesis on this, here's some bonus docs: http://www.cse.chalmers.se/edu/year/2011/course/TDA361_Computer_Graphics/grid.pdf
		//ok, so I lied, at least this version has comments unlike past version I've found.

		//we need to know what grid we are in.
		Vector3i currGrid = startPos;
		//we need to know our rays current position when incrementing, the 'partials' e.g. the decimals, and the deltas it would take to move along the ray
		//what that basically means is, we try to figure out how much of the distance we would use up if we tried to move in the grid co-ordinates of x/y/z
		Vector3f currPos, partials, deltas;
		currPos = startPos;
		//keep track of our total movement for this grid (dt), and our total useage of the distance (currT)
		float dt, currT;
		currT = 0;
		//we need a way of determining how far we must move in a direction to be to the next voxel boundary
		//if the number is positive e.g. in that direction we are moving 'up' we need to check to the upper bound
		//if we are moving negative, we need to check the lower bound, hence for positive 1, and negative, 0
		//Vector3f step(dir.x > 0 ? 1 : 0, dir.y > 0 ? 1 : 0, dir.z > 0 ? 1 : 0);
		while (currT < maxDist)
		{
			partials = getPartials(currPos, dir);
			//prevent an infinite loop when incrementing in the negative direction
			partials = Vector3f(partials.x == 0 ? 1 : partials.x, partials.y == 0 ? 1 : partials.y, partials.z == 0 ? 1 : partials.z);
			//how far along the line we'd have to move for x/y/z to hit their respective boundaries, check for lower.
			//and since in some cases that could be 'negative infinity' we take the absolute value.
			deltas = Vector3f::vec_abs(partials / dir);
			//find out what direction results in the lowest increment of dt, and set dt to that.
			if (deltas.x < deltas.y)
			{
				if (deltas.z < deltas.x)
					dt = deltas.z;
				else
					dt = deltas.x;
			}
			else
			{
				if (deltas.z < deltas.y)
					dt = deltas.z;
				else
					dt = deltas.y;
			}
			currT += dt;
			//we can wind up checking past our ray, thus we must exit out of any extra calculations.
			if (currT >= maxDist)
			{
				break;
			}
			lastPos = currGrid;
			//we increment based on dt so we move in the direction of our vector.. I'm honestly not sure why I'm comenting this.
			currPos += dir*dt;
			currGrid = OffsetFix(dir,currPos);
			map.GetBlock(currGrid, hit, block);
			if (!ignoreColliders)
			{
				if (MapData::blocks[block].solid && block != Chunk::Air_Block)
				{
					hit = true;
					hitPos = currGrid;
					return;
				}
			}
			else
			{
				if (block != Chunk::Air_Block)
				{
					hit = true;
					hitPos = currGrid;
					return;
				}
			}
		}
		//we didn't find a block.
		hit = false;
		hitPos = Vector3f(0, 0, 0);
		lastPos = hitPos;
	}

	Vector3i BlockCast::OffsetFix(Vector3f step, Vector3f vecPos)
	{
		/*
		there is an issue with standard flooring if you have a ray moving in the not +,+,+ direction, nor -,-,- e.g. +,-,- or +,+,- etc
		the rounding issue causes points to be skipped/counted twice.
		if the direction is negative in that component, and 1 | 0 other components are also negative decrement that grid to correctly access proper locations.
		-------------------------------------------------------------------------
		|	*	|		|		|		|		|		|		|		|		|
		|	  *	|		|		|		|		|		|		|		|		|
		---------*---------------------------------------------------------------
		|		|  *	|		|		|		|		|		|		|		|
		|		|	  *	|		|		|		|		|		|		|		|
		-----------------*-------------------------------------------------------
		|		|		|   *	|		|		|		|		|		|		|
		|		|		|		*		|		|		|		|		|		|
		----------------------------*--------------------------------------------
		as a terrible example, since I can't just screen cap an example in code..
		*/
		Vector3i pos = Vector3f::floor(vecPos);
		if (fabs(fmod(vecPos.x, 1)) <= 0.0001f)
		{
			if (step.x <= 0 && !(step.y <=0 && step.z <=0))
			{
				--pos.x;
			}
		}
		if (fabs(fmod(vecPos.y, 1)) <= 0.0001f)
		{
			if (step.y <= 0  && !(step.x <= 0 && step.z <= 0))
			{
				--pos.y;
			}
		}
		if (fabs(fmod(vecPos.z, 1)) <= 0.0001f)
		{
			if (step.z <= 0 && !(step.y <= 0 && step.x <= 0))
			{
				--pos.z;
			}
		}
		return pos;
	}
	Vector3f BlockCast::getPartials(Vector3f gridPos, Vector3f dir)
	{
		return Vector3f(getPartialPiece(gridPos.x, dir.x), getPartialPiece(gridPos.y, dir.y), getPartialPiece(gridPos.z, dir.z));
	}
	float BlockCast::getPartialPiece(float pos, float dir)
	{
		// due to some 'fun' ways in which negative numbers can be a pain in the ass...
		
		//we need to determine how far we must move until we are at the next block,
		//if the grid co-ord is positive, and the direction is also positive we have to move 'up'
		//if the direction was negative, we must move down.
		//if the position is negative, invert the entire process.
		//fabs is required due to fmod() returning negative(s) when we are trying to get just the decimal.
		//we could probably switch this to modf() and just get the decimal instead, but I haven't bothered switching/testing speed yet so..
		if (pos > 0)
		{
			if (dir > 0)
			{
				return 1 - (float)fabs(fmod(pos, 1));
			}
			else
			{
				return (float)fabs(fmod(pos, 1));
			}
		}
		else
		{
			if (dir > 0)
			{
				return (float)fabs(fmod(pos, 1));
			}
			else
			{
				return 1 - (float)fabs(fmod(pos, 1));
			}
		}
	}
}
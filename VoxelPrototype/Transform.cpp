#include "Transform.h"
#include "GameObject.h"
bool Transform::toggleBool = false;

Transform::Transform(Vector3f position, Vector3f scale, Vector3f eulerAngles)
{
	this->position = position;
	this->scale = scale;
	this->eulerAngles = eulerAngles;
	this->regenChildren = true;
	this->regenLocalMatrix = true;
	this->regenWorldMatrix = true;
	this->stateBool = !Transform::toggleBool;
	parent = NULL;
	regenLocalMatrix = true;

}

void Transform::UpdateLocalMatrix()
{
	if (regenLocalMatrix)
	{
		Matrix4x4 scale(scale);
		Matrix4x4 translate = Matrix4x4::makeTranslate(position);
		eulerAngles = Vector3f::vec_mod(eulerAngles, Vector3f(2 * PI, 2 * PI, 2 * PI));
		Matrix4x4 rotation = Matrix4x4::makeRotationZ(eulerAngles.z)*Matrix4x4::makeRotationX(eulerAngles.x)*Matrix4x4::makeRotationY(eulerAngles.y);
		Matrix4x4 result = scale*rotation*translate;
		//the logic order: get the scale, rotate it, then translate it.
		//not sure where scales supposed to go entirely...
		regenLocalMatrix = false;
		localMatrix = result;
	}
}
void Transform::UpdateWorldMatrix()
{
	//what this tries to do, is to kill off the exponential case of recursion when dealing with our tree.
	//if the parent of a node has regened it's local/world matrix, we should regen
	//tell the children they need to regen if we've regened, and don't alter their regen state until the next frame (removes checking to see how many children have regened and more complex things)
	//the world matrix should be stored after calculating for fast access incase a child needs it, we need it a new frame, and we haven't changed.
	//determine if we are on the same frame or not, if we are, store that flag and just supply the matrix so the caller can determine what our world matrix is.
	if (toggleBool != stateBool)
	{
		stateBool = toggleBool;
		if (parent == NULL)
		{
			if (regenWorldMatrix)
			{
				UpdateLocalMatrix();
				worldMatrix = localMatrix;
				regenWorldMatrix = false;
				regenChildren = true;
			}
			else
			{
				regenChildren = false;
			}
		}
		else
		{
			if (regenWorldMatrix || parent->regenChildren)
			{
				UpdateLocalMatrix();
				parent->UpdateWorldMatrix();

				worldMatrix = localMatrix*parent->worldMatrix;
				regenWorldMatrix = false;
				regenChildren = true;
			}
			else
			{
				regenChildren = false;
			}
		}
	}
}


void Transform::Move(Vector3f dist)
{
	position += localMatrix*dist;
	regenLocalMatrix = true;
	regenWorldMatrix = true;
}
Vector3f Transform::localPosition()
{
	return Vector3f(localMatrix.m14, localMatrix.m24, localMatrix.m34);
}
Vector3f Transform::worldPosition()
{
	return Vector3f(worldMatrix.m14, worldMatrix.m24, worldMatrix.m34);
}
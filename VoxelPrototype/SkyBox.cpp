#include "SkyBox.h"
#include "Model.h"
#include "Transform.h"
SkyBox::SkyBox()
{
}

SkyBox::SkyBox(shared_ptr<Shader> shader)
{
	this->shader = shader;																								//Temp stuff
	transform = shared_ptr<Transform>(new Transform(Vector3f(0, 0, 0), Vector3f(1, 1, 1), Vector3f(0, 0, 0)));			//Temp stuff
	model = shared_ptr<Model>(new Model(shader, transform));															//Temp stuff


	genVerts();
	transform->scale = Vector3f(10, 10, 10);
}
SkyBox::~SkyBox()
{
}
void SkyBox::Init()
{
	model = shared_ptr<Model>(new Model(shader, transform));
	genVerts();
}
void SkyBox::Render()
{
	//if this looks damn similar to the regular render, its meant to, I'm making sure we can change cubemaps without it fucking up
	//unlike jeffs lab <_<
	//disable the depth testing
	glDepthMask(GL_FALSE);
	glActiveTexture(GL_TEXTURE0);
	glUniform1i(cubemap.cubemapID, 0);
	//(model->textureLoc, 0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap.textureID);
	model->Render();

	//renable depth testing
	glDepthMask(GL_TRUE);
}
void SkyBox::SetCubeMap(CubeMap& map)
{
	this->cubemap = map;
}
void SkyBox::genVerts()
{
	//num of floats to specify 3d, and number of points in a cube (8)
	vector<GLfloat> vertices = { -10, -10, -10,	//0
				-10, -10, 10,		//1
				-10, 10, -10,		//2
				-10, 10, 10,		//3
				10, -10, -10,		//4
				10, -10, 10,		//5	
				10, 10, -10,		//6
				10, 10, 10 };		//7
	//wind the strips clockwise (or rather, opposite of normal, so that they draw correctly since we are inside the cube at all times.
	//num of triangles made: 2*face aka 12 sizeof(indices) = 36
	vector<GLuint> indices = {	0, 3, 1,
				0, 2, 3,	//face 0 'out' left

				4, 5, 7,
				4, 7, 6,	//face 1 'in' right

				2, 7, 3,
				2, 6, 7,	//face 2 'out' top

				0, 1, 5,
				0, 5, 4,	//face 3 'out' bottom

				0, 4, 6,
				0, 6, 2,	//face 4 'in' back

				1, 7, 5,
				1, 3, 7 };	//face 5 'out' front		
				
	model->setGeometry(vertices, (unsigned int)vertices.size());
	model->setIndexBuffer(indices, (unsigned int)indices.size());
	model->loadVBO();
}

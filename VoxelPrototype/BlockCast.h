#pragma once
#include "VoxelLib.h"
//use it like a raycast, its meant to work on a square grid, and currently does not support rotation of the supplied map
namespace Voxel
{
	class Map;
	class BlockCast
	{
	public:
		//the amount of steps to perform per 1 float unit, higher = more precision, but more time consuming. lower = faster etc etc
		const static int stepSize = 100;
		//specify a starting pos, a direction, the max distance the ray can travel and supply it a map to look up against.
		//if a block was hit along the distance, 'hit' will be true, else false.
		//assuming a block was hit, it will return the position of the block hit, and the position of the prior 'block' it was in.
		//if ignore colliders is turned on, the ray will hit non solid blocks (except air)
		static void Cast(Vector3f startPos, Vector3f direction, float maxDist, bool& hit, Vector3i& hitPos, Vector3i& lastPos, Map& map, bool ignoreColliders);
	private:
		static Vector3i OffsetFix(Vector3f step, Vector3f vecPos);
		static Vector3f getPartials(Vector3f gridPos, Vector3f dir);
		static float getPartialPiece(float pos, float dir);
	};
}
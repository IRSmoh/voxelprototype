#pragma once
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "Shader.h"
class Light
{
public:
	Light(GLfloat* position, GLfloat* ambient, GLfloat* diffuse, GLfloat* specular, Shader* shader);
	void setLightPositionInShader();
	void setLightPosition(GLfloat* newPosition);

	GLfloat* position;
	GLfloat* ambient;
	GLfloat* diffuse;
	GLfloat* specular;
	Shader* shader;
	~Light();
private:
	GLuint lightPositionID;
};


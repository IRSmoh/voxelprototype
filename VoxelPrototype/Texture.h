#pragma once
#include <vector>
#include "lodepng.h"
#include <GL\glew.h>
#include <GLFW\glfw3.h>
using namespace std;
//stores a single texture on the gpu, destroying the instance destroy the image on the gpu.
class Texture
{
#define NO_IMAGE  0xFFFFFFFF
protected:
	unsigned int error;
	vector<unsigned char> image;
	int channelType;
public:
	unsigned int width, height;
	GLuint textureID;

	GLint textureType;
	Texture();
	Texture(char* filename, int channelType);
	//if there was an error, return it. if NO_IMAGE Texture was created via default constructor
	unsigned int GetError();
	//pushes the texture to the GPU shouldn't make duplicates(?)
	virtual void PushTexture(GLint shaderID);
	virtual ~Texture();
	void TestingBindlessGL();
};


#include "FileIO.h"
//fuck off microsoft.
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
void FileIO::mkdirRecursive(std::string& path)
{
	//attempt to make the folder, if it fails make sure it's because the folder wasn't made.
#if defined _MSC_VER
	if (_mkdir(path.c_str()) && errno == ENOENT)
#else
	//confirm this is the correct format for linux.
	if (mkdir(path.c_str()) && errno == ENOENT)
#endif
	{
		//we couldn't make the folder so try and build the next highest level folder
		size_t pos = path.rfind("\\");
		mkdirRecursive(path.substr(0, pos));
#if defined _MSC_VER
		_mkdir(path.c_str());
#else
		mkdir(path.c_str());
#endif
	}
}
FILE* FileIO::openFile(std::string path, std::string& name, const char* args)
{
	mkdirRecursive(path);
	FILE* file = fopen((path + (path.empty() ? "" : "\\") + name).c_str(), args);
	if (file)
	{
		return file;
	}
	return nullptr;
}
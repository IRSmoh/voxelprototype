﻿#include "Texture.h"

Texture::Texture()
{
	width = height = 0;
	error = NO_IMAGE;
	textureID = -1;
	channelType = GL_RGBA;
	textureType = GL_TEXTURE_2D;
}
Texture::Texture(char* filename, int channelType)
{
	error = lodepng::decode(image, width, height, filename);
	textureID = -1;
	this->channelType = channelType;
	textureType = GL_TEXTURE_2D;
}
Texture::~Texture()
{
	glDeleteTextures(1,&textureID);
}
unsigned int Texture::GetError()
{
	return error;
}

//Jeffs tutorials are highly inconsistent at best/mostly terrible practices now that I look at them, I'm commenting this out and going to think about a proper setup later and/or how this should function.
void Texture::PushTexture(GLint shaderID)
{
	//I know I'll forget this later, so I'm more of commenting to remember wtf this stuff does.
	//enable using textures (in general)
	glEnable(textureType);
	//give us an object on the GPU (it's empty atm)
	glGenTextures(1, &textureID);
	glEnable(textureID);
	//tell the graphics card we're using the ID/object we just created
	glBindTexture(textureType, textureID);
	//push the texture
	glTexImage2D(textureType, 0, channelType, width, height, 0, channelType, GL_UNSIGNED_BYTE, &image[0]);
	//clear the image data, we don't need it stored here any longer. this leaves 16 bytes of overhead vs 16 + sizeof image
	image.clear();
	glGenerateMipmap(textureType);

	//set up the repeats
	glTexParameteri(textureType, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(textureType, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//not mipmapped
	glTexParameteri(textureType, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//mipmapped.
	glTexParameteri(textureType, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
}

void Texture::TestingBindlessGL()
{
	GLuint64 texAddress = glGetTextureHandleARB(textureID);
}

#pragma once
#include "VoxelLib.h"
namespace Voxel
{
	//control which chunks are loaded, rendered, pass off the various cameras to the chunks
	//remesh a chunk (mark as dirty from the chunk -> place in mesh list)
	//have various lists for: loading, unloading, meshing, building, 
	class ChunkManager
	{
	public:
		Region* region;
		Transform* world_transform;
		bool wireframe : 1;
		bool meshing : 1;
		ChunkHash Chunks;							//hash table to store all our nearby chunks, dirtyChunks/renderList will pull from this
		RegionManager regionManager;
		HeightMapHash heightMap;
		ChunkManager();
		//length of one side of the map note: render distance 1/2 this
		ChunkManager(int mapSide, std::shared_ptr<Shader> shader, Transform* world_transform);
		ChunkManager(const ChunkManager&) = delete;	//condition variable/other things bricks this.
		ChunkManager& operator=(const ChunkManager&) = delete;	//for the same reason as the copy constructor
		~ChunkManager();
		//supply a world position, if a chunk can be found that owns the world pos, it will be returned.
		Chunk* GetOwningChunk(Vector3i blockPos);
		//if the chunk is not loaded, causes it to be generated/loaded.
		void ForceLoaded(Vector3i pos);
		void StoreChunk(Chunk& chunk);
		void LoadChunk(Vector3i chunkPos);
		//supply a position, and if this position lies >1 chunk outside the current center of the loaded chunks, it will load the missing chunks.
		void LoadNearbyChunks(Vector3i basePos);
		void UpdateRenderable();
		//handles all the above methods, they most likely will become private at some point, and this would be the only thing called.
		void Update();
		void RenderChunks();
	private:
		int chunkDim;
		std::condition_variable cv_main;
		std::condition_variable cv_mesh;
		std::condition_variable cv_build;
		std::atomic_uint8_t thread_counter;
		unsigned char threadCount;
		bool exitThreads;
		bool notified;
		std::shared_ptr<Shader> shader;

		std::vector<Chunk*> dirtyChunksList;		//chunks that need to be remeshed
		std::vector<Chunk*> buildChunksList;		//chunks that are being built for first time once built, they should be placed in playerChunks
		std::vector<Vector3i> loadList;						//chunks that need to be loaded / loads from file dumps into dirtyChunksList

		//build or mesh the chunks as needed, this is threaded, and frequently is waiting for requests.
		static void Build_Mesh(ChunkManager* manager);
		//threadable
		static void GenMeshes(std::vector<Chunk*>* startPos, std::vector<int>* work, ChunkManager* manager, int threadID);
		//threadable
		static void BuildChunks(std::vector<Chunk*>* startPos, std::vector<int>* work, ChunkManager* manager, int threadID);
	};
}
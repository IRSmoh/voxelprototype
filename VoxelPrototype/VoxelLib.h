#pragma once

typedef unsigned short BlockID;
//basic GL reqs
#include <GL\glew.h>
#include <GLFW\glfw3.h>

//basic STL requirements
#include <vector>
#include <memory>
#include <assert.h>
#include <string>
#include <tuple>
#include <condition_variable>
#include <atomic>
#include <thread>
#include <mutex>
#include "FileIO.h"
#include "Keys.h"
#include "Deleter.h"
#include "RLcompression.h"
#include "Timer.h"

//basic maths
#include "Matrix4x4.h"
#include "Vector3f.h"
#include "Vector3i.h"

//standard use things
#include "Shader.h"
#include "Transform.h"
#include "Model.h"

//constants used too often
const static int SOUTH = 1;
const static int NORTH = 0;
const static int EAST = 2;
const static int WEST = 3;
const static int TOP = 4;
const static int BOTTOM = 5;

//basic block constructs
#include "Block.h"

//world gen
#include "OpenSimplexNoise.h"
#include "HeightMap.h"
#include "HeightMapHash.h"
#include "WorldGen.h"

//basic chunk constructs
#include "VoxelModelData.h"
#include "VoxelMesh.h"
#include "Chunk.h"
#include "ChunkHash.h"
#include "Texture2DArray.h"

//basic map constructs
#include "Region.h"
#include "RegionManager.h"
#include "ChunkManager.h"
#include "VoxelMapData.h"
#include "VoxelMap.h"


//helper functions
#include "BlockCast.h"

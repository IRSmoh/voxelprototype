#pragma once
#include "VoxelLib.h"
namespace Voxel
{
	class ChunkManager;
	//stores an array of blocks (32^3)
	//uses a form of greedy meshing to create a valid mesh for all the blocks
	//that mesh must be pushed to a proper model class for it to be rendered, this class does no rendering.
	class Chunk
	{
	public:
		const static unsigned char Chunk_BitShift = 5;					//not a massive need, but useful to sync things that require bitshifting, aka accessing the chunk mostly.
		const static unsigned char Chunk_Side = 1 << Chunk_BitShift;	//the size of the chunk is Chunk_Side^3
		const static unsigned int Chunk_BitMask = Chunk_Side - 1;	//use this to determine the correct array offset for a chunk, (arbitrary sizes are valid, e.g. -2 bill -> +2 bill
		const static unsigned short chunkSize = Chunk_Side*Chunk_Side*Chunk_Side;
		const static BlockID Air_Block = 0;						//the "empty" block, could be defined differently depending on map e.g. space ignored when meshing (gens no mesh)
		const static unsigned char numNeighborChunks = 6;
		std::shared_ptr<Transform> transform;	//temp stuff
		std::shared_ptr<Model> model;			//temp stuff

		ChunkManager* manager;

		bool isDirty : 1;	//marks if the chunk needs to be remeshed
		bool isStale : 1;	//this flag is used to mark if the chunk needs to be restored to disk/region file
		bool loadVBO : 1;	//tell sus the chunk has finished meshing, and needs to have its data passed to the gpu
		bool isNew : 1;		//an initilizer flag to help with loading a new area/world/etc
		Vector3i arrayPos;										//our access position into the chunkhash 
		std::shared_ptr<BlockID> Blocks;						//unsigned short = 65k different blocks, that's more than plenty and 1/2 the size of an int 16bit vs 32bit

		Chunk(std::shared_ptr<Shader> shader, Vector3i arrayPos, ChunkManager* manager);
		Chunk();
		virtual ~Chunk();
		//chunk related things
		void BuildChunk();
		//used for loading in a chunk, not used yet
		void AssignBlocks(std::shared_ptr<BlockID> Blocks);
		void BuildMesh();
		void Render();

		void PlaceBlock(BlockID block, Vector3i pos);
		//range is x: 0-15, y:0-15, z: 0-15
		BlockID& operator[](Vector3i index);
		void DirtyAllNeighbors();
	protected:
		std::shared_ptr<VoxelMesh> tempMesh;
		//the logic here is to store the offset in mapData->blocks to get what type of block is in the chunk
		void Quad(Vector3f& bottomLeft, Vector3f& topLeft, Vector3f& topRight, Vector3f& bottomRight, int width, int height, BlockID voxel, bool backFace, int side);
		void GreedyMesh();
		Chunk** FetchNeighbors(); //call delete on the returned array.
		//checks to make sure the input co-ords does not need to update a neighbor for meshing.
		void DirtyNeighbors(Vector3i pos);
	};
}

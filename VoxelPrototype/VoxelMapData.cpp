#include "VoxelLib.h"
namespace Voxel
{
	std::vector<Block> MapData::blocks = std::vector<Block>(1, Block(0, true, false, "Air"));
	MapData::mapSeed MapData::seed = 0;
	Texture2DArray MapData::voxelTexts;

	void MapData::InitVoxelTexts(Shader* shader)
	{
		const char* stone = "Textures\\sunken.png";
		const char* quartz = "Textures\\quartz.png";
		const char* dirtTop = "Textures\\dirt_top.png";
		const char* dirtSide = "Textures\\dirt_side.png";
		const char* dirtBottom = "Textures\\dirt.png";
		voxelTexts = Texture2DArray(GL_RGBA, 5);
		//unsigned int error = voxelTexts.GetError();
		voxelTexts.AddTexture(stone);
		voxelTexts.AddTexture(quartz);
		voxelTexts.AddTexture(dirtTop);
		voxelTexts.AddTexture(dirtSide);
		voxelTexts.AddTexture(dirtBottom);
		voxelTexts.FinalizeTexture(shader->programID);
	}
	void MapData::Init(mapSeed _seed)
	{
		//the only reason I'm adding this extension is so it gets ignored.
		string seed_filename = "seed.spawn_region";
		string path = "Saves";
		auto data = FileIO::loadFromDisk_managed<mapSeed>(path, seed_filename);
		if (get<0>(data) != nullptr)
		{
			seed = get<0>(data).get()[0];
		}
		else
		{
			seed = _seed;
			FileIO::storeToDisk(path, seed_filename, &seed, 1, true);
		}
		WorldGen::noise3D = OSN::Noise<3>(seed);
		WorldGen::noise2D = OSN::Noise<2>(seed + 1);
		DefaultTest();
	}
	void MapData::addBlockType(Block block)
	{
		blocks.push_back(block);
	}
	unsigned int MapData::findBlockTypeIndex(std::string& blockName)
	{
		unsigned int failed = -1;
		for (unsigned int i = 0; i < blocks.size(); i++)
		{
			if (blocks[i].name == blockName)
			{
				return i;
			}
		}
		return failed;
	}
	void MapData::DefaultTest()
	{
		Block stone(1, false, true, "Stone", 0, 0, 0, 0, 0, 0);
		Block glass(2, false, true, "Quartz", 1, 1, 1, 1, 1, 1);
		Block dirtRubbish(3, false, true, "DirtRubbish", 3, 3, 3, 3, 2, 4);
		Block dirt(4, false, true, "Dirt", 4, 4, 4, 4, 4, 4);
		blocks.push_back(stone);
		blocks.push_back(glass);
		blocks.push_back(dirtRubbish);
		blocks.push_back(dirt);
	}
}
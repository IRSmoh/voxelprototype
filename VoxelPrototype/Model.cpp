#include "Model.h"

#define BUFFER_OFFSET(i) ((char *)NULL + (i))
GLuint Model::lastShader = -1;
Model::Model()
{
	this->transform = NULL;									//Temp Stuff
	uvIndivialSize = 2;
}
Model::Model(shared_ptr<Shader> shader)
{
	this->transform = NULL;									//Temp Stuff
	this->shader = shader->programID;
	glGenVertexArrays(1, &vao);								// Generate a VAO
	glGenBuffers(1, &vbo);									// Generate a buffer object
	glGenBuffers(1, &index_vbo);

	vertices = vector<GLfloat>();
	normals = vector<GLfloat>();
	uvs = vector<GLfloat>();
	colors = vector<GLfloat>();
	indices = vector<GLuint>();
	vertexSize = indexSize = 0;

	vPosition = vNormal = vTexCoord = vColor = umM = -1;

	wireframe = false;
	umM = glGetUniformLocation(shader->programID, "model");	// Find the mM variable in the shader 

	uvIndivialSize = 2;
}
Model::Model(shared_ptr<Shader> shader, shared_ptr<Transform> transform)
{
	this->transform = transform;							//Temp Stuff
	this->shader = shader->programID;
	glGenVertexArrays(1, &vao);								// Generate a VAO
	glGenBuffers(1, &vbo);									// Generate a buffer object
	glGenBuffers(1, &index_vbo);

	vertices = vector<GLfloat>();
	normals = vector<GLfloat>();
	uvs = vector<GLfloat>();
	colors = vector<GLfloat>();
	indices = vector<GLuint>();
	vertexSize = indexSize = 0;

	vPosition = vNormal = vTexCoord = vColor = umM = -1;

	wireframe = false;
	transform->scale = Vector3f(1, 1, 1);
	umM = glGetUniformLocation(shader->programID, "model");	// Find the mM variable in the shader 

	uvIndivialSize = 2;
}
Model::~Model()
{
	glDisableVertexAttribArray(vPosition);					// Cleanup the mess we've made
	glDisableVertexAttribArray(vNormal);
	glDisableVertexAttribArray(vTexCoord);
	glDisableVertexAttribArray(vColor);
	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
}
void Model::setGeometry(vector<GLfloat>& vertices, unsigned int size)
{
	if (&this->vertices != &vertices)
		this->vertices.swap(vertices);
	this->vertexSize = size / 3;
}
void Model::setNormal(vector<GLfloat>& normals, unsigned int size)
{
	if (&this->normals != &normals)
		this->normals.swap(normals);
}
void Model::setTextureCoordinates(vector<GLfloat>& uvs, unsigned int size)
{
	if (&this->uvs != &uvs)
		this->uvs.swap(uvs);
}
void Model::setVertexColors(vector<GLfloat>& colors, unsigned int size)
{
	if (&this->colors != &colors)
		this->colors.swap(colors);
}
void Model::setIndexBuffer(vector<GLuint>& indices, unsigned int size)
{
	if (&this->indices != &indices)
		this->indices.swap(indices);
	this->indexSize = size;
}
void Model::SetTexture(Texture* text)
{
	this->text = text;
}
void Model::loadVBO()
{
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	int offset = 0;
	int sizeBuffer = (vertexSize * 3 + normals.size() + uvs.size() + colors.size())*sizeof(GLfloat);
	glBufferData(GL_ARRAY_BUFFER, sizeBuffer, NULL, GL_STATIC_DRAW);
	//3 values for each vertex
	if (vertexSize)
	{
		if (vPosition == -1)
			vPosition = glGetAttribLocation(shader, "vPosition");
		glBufferSubData(GL_ARRAY_BUFFER, offset, vertexSize * 3 * sizeof(GLfloat), &vertices[0]);
		glVertexAttribPointer(vPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);
		offset += vertexSize * 3 * sizeof(GLfloat);
		vertices.clear();
		vertices.shrink_to_fit();
	}
	//3 values for each normals
	if (!normals.empty())
	{
		if (vNormal == -1)
			vNormal = glGetAttribLocation(shader, "vNormal");
		glBufferSubData(GL_ARRAY_BUFFER, offset, normals.size() * sizeof(GLfloat), &normals[0]);
		glVertexAttribPointer(vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(offset));
		offset += normals.size() * sizeof(GLfloat);
		normals.clear();
		normals.shrink_to_fit();
	}
	//2 values by default for each uv
	if (!uvs.empty())
	{
		if (vTexCoord == -1)
			vTexCoord = glGetAttribLocation(shader, "vTexCoord");
		glBufferSubData(GL_ARRAY_BUFFER, offset, uvs.size() * sizeof(GLfloat), &uvs[0]);
		glVertexAttribPointer(vTexCoord, uvIndivialSize, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(offset));
		offset += uvs.size() * sizeof(GLfloat);
		uvs.clear();
		uvs.shrink_to_fit();
	}
	//4 values per color, rgba
	if (colors.size())
	{
		if (vColor == -1)
			vColor = glGetAttribLocation(shader, "vColor");
		glBufferSubData(GL_ARRAY_BUFFER, offset, colors.size() * sizeof(GLfloat), &colors[0]);
		glVertexAttribPointer(vColor, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(offset));
		offset += colors.size() * sizeof(GLfloat);
		colors.clear();
		colors.shrink_to_fit();
	}
	if (indexSize)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_vbo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexSize*sizeof(GLuint), &indices[0], GL_STATIC_DRAW);
		indices.clear();
		indices.shrink_to_fit();
	}
}
void Model::Render()
{
	glBindVertexArray(vao);
	if (shader != lastShader)
	{
		glUseProgram(shader);
		lastShader = shader;
	}
	//there can be cases where we want this to be null, it doens't make a lot of sense now.. but we might.
	if (transform)							//Temp stuff
		transform->UpdateWorldMatrix();

	if (!wireframe)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	if (vPosition !=-1)
	{
		glEnableVertexAttribArray(vPosition);
	}
	if (vNormal !=-1)
	{
		glEnableVertexAttribArray(vNormal);
	}
	if (vTexCoord != -1)
	{
		glEnableVertexAttribArray(vTexCoord);
		//bind texture for use
		glBindTexture(text->textureType, text->textureID);
	}
	if (vColor != -1)
	{
		glEnableVertexAttribArray(vColor);
	}
	if (umM != -1 && transform)												//Temp stuff
	{
		glUniformMatrix4fv(umM, 1, GL_FALSE, &transform->worldMatrix[0]);  //temp stuff 
	}
	if (indexSize == 0)
	{
		if (vPosition != -1)
		{
			glBindBuffer(GL_ARRAY_BUFFER, vbo);
			glDrawArrays(GL_TRIANGLES, 0, vertexSize / sizeof(GLfloat) / 3);
		}
	}
	else
	{
		//if using an index buffer remember, each index counts to a pair of vertices/normals/uvs/colors. aka make enough pairings that the index is valid for all cases. in short, generate your data correctly.
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_vbo);
		glDrawElements(GL_TRIANGLES, indexSize, GL_UNSIGNED_INT, NULL);
	}

	//disable everything for next draw
	if (vPosition != -1)
	{
		glDisableVertexAttribArray(vPosition);
	}
	if (vNormal != -1)
	{
		glDisableVertexAttribArray(vNormal);
	}
	if (vTexCoord != -1)
	{
		glDisableVertexAttribArray(vTexCoord);
	}
	if (vColor != -1)
	{
		glDisableVertexAttribArray(vColor);
	}
}

#pragma once
#include <vector>
#include <string>
#include <memory>
#if defined _MSC_VER
#include <Windows.h>
#include <direct.h>
#elif defined __GNUC__
#include <sys/types.h>
#include <sys/stat.h>
#endif
class FileIO
{
public:
	//linear in number of folders created (faster than system("mkdir <folders>"))
	static void mkdirRecursive(std::string& path);
	//you have to close the returned FILE* assuming it's not null.
	static FILE* openFile(std::string path, std::string& name, const char* args);

	template<typename T>
	static bool storeToDisk(std::string path, std::string filename, T* data, size_t size, bool forceFolderCreation);

	template<typename T>
	static std::tuple<std::shared_ptr<T>, size_t> loadFromDisk_managed(std::string path, std::string filename);
	//in some cases we don't want the thing auto managed.. e.g. when we have to realloc a malloc'd block of data.
	//this allows us to do that, instead of forcing us to use a shared_ptr
	template<typename T>
	static std::tuple<T*, size_t> loadFromDisk_nonmanaged(std::string path, std::string filename);
};

/* why isn't this in the cpp? because visual studio is too stupid to find it otherwise.*/
template<typename T>
std::tuple<std::shared_ptr<T>, size_t> FileIO::loadFromDisk_managed(std::string path, std::string filename)
{
	std::tuple<T*, size_t> tmp = loadFromDisk_nonmanaged<T>(path, filename);
	return std::tuple<std::shared_ptr<T>, size_t>(shared_ptr<T>(get<0>(tmp), free), get<1>(tmp));
}
template<typename T>
std::tuple<T*, size_t> FileIO::loadFromDisk_nonmanaged(std::string path, std::string filename)
{
	FILE* file = NULL;
	file = fopen((path + (path.empty() ? "" : "\\") + filename).c_str(), "rb");
	if (file == NULL)
	{
		//failed to find file, specify garbage.
		return std::tuple<T*, size_t>(nullptr, 0);
	}
	//the file is valid, read it's data and parse.
	fseek(file, 0, SEEK_END);
	long file_length = ftell(file);
	//go back to the begining of the file so we can read our data.
	fseek(file, 0, SEEK_SET);
	T* data = (T*)malloc(file_length);
	//normally you are supposed to have the byte size instead of 1, but since it's a void pointer the algo honestly doesn't care about the size.
	//and we have a properly sized buffer, so fuck it, store the data directly and let us manipulate it.
	fread(data, 1, file_length, file);
	fclose(file);
	//and we're done here. specify our data, and the number of elements.
	return std::tuple<T*, size_t>(data, file_length / sizeof(T));
}
template<typename T>
bool FileIO::storeToDisk(std::string path, std::string filename, T* data, size_t size, bool forceFolderCreation)
{
	FILE* file = NULL;
	if (!forceFolderCreation)
	{
		file = fopen((path + (path.empty() ? "" : "\\") + filename).c_str(), "wb");	//replace with something that will create the subfolders
	}
	else
	{
		mkdirRecursive(path);
		file = fopen((path + (path.empty() ? "" : "\\") + filename).c_str(), "wb");
	}
	if (file == NULL)
	{
		//you fucked up, attempt to log the file path if possible, but we don't have logs right now so..
		std::cout << "Failed to store file: " << (path + (path.empty() ? "" : "\\") + filename) << std::endl;
		return false;
	}
	//the reason we specify the size of the data here is due to size indicating the number of elements, not the size of the buffer.
	fwrite(data, sizeof(T), size, file);
	fclose(file);
	return true;
}
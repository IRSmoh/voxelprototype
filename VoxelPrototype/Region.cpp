#include "VoxelLib.h"
namespace Voxel
{
#define GET_CHUNK_INFO(x,y,z) chunkStats[(x)+(y)*sectorDim + (z)*sectorDim*sectorDim]
#define GET_ARRAY_POS(x,y,z) (x) + (y)*sectorDim + (z)*sectorDim*sectorDim
	Region::Region()
	{
		//layout
		// implied: [0,1,2,3,4] etc for chunk numbers.
		//---------------------------------------------------
		//|chunkSize|chunkSize|chunkSize|chunkSize|chunkSize|
		//--------------------------------------------------------
		//|chunkStart|chunkStart|chunkStart|chunkStart|chunkStart|
		//--------------------------------------------------------
		//|chunk0|chunk1|chunk2|chunk3|chunk4|etc
		//---------------------------------------
		//this is to just set up the base size.
		memSize = headerSize + cubicSector*sectorSize /*the actual chunks*/;
		block = calloc(memSize, sizeof(char));
		//the starting point is the begining of the memory block.
		chunkStats = (ChunkInfo*)block;
		//could we remove the type casting and instead infer the size? yeah, does it make it more readable? I'll make up my mind later.
		chunks = (BlockID*)(chunkStats + cubicSector);
		//init our header to reasonable values.
		for (int i = 0; i < cubicSector; i++)
		{
			chunkStats[i].chunkOffset = i*sectorSize;
			chunkStats[i].chunkSize = 0;
		}
		lastUpdate = 0;
		isStale = false;
	}
	Region::Region(Vector3i pos)
	{
		region_pos = pos;
		memSize = headerSize + cubicSector*sectorSize /*the actual chunks*/;
		block = calloc(memSize, sizeof(char));
		//the starting point is the begining of the memory block.
		chunkStats = (ChunkInfo*)block;
		//could we remove the type casting and instead infer the size? yeah, does it make it more readable? I'll make up my mind later.
		chunks = (BlockID*)(chunkStats + cubicSector);
		//init our header to reasonable values.
		for (int i = 0; i < cubicSector; i++)
		{
			chunkStats[i].chunkOffset = i*sectorSize;
			chunkStats[i].chunkSize = 0;
		}
		lastUpdate = 0;
		isStale = false;
	}
	Region::~Region()
	{
		free(block);
	}
	std::tuple<BlockID*, unsigned short> Region::GetChunk(Vector3i pos)
	{
		if (pos.x < 0 || pos.z < 0 || pos.y < 0 || pos.x > 16 || pos.z > 16 || pos.y > 16)
			return std::tuple<BlockID*, unsigned short>(nullptr, 0);
		//so we've made sure we aren't trying to access an invalid location.
		//find the chunk
		//find it's starting position
		//give us the chunk, and report it's size
		//the decompression is left up to something else (other method in this class/other class/etc)
		ChunkInfo stats = GET_CHUNK_INFO(pos.x, pos.y, pos.z);
		return std::tuple<BlockID*, unsigned short>((BlockID*)((char*)chunks + stats.chunkOffset), stats.chunkSize);
	}
	Region::ChunkInfo Region::GetChunkInfo(Vector3i pos)
	{
		return GET_CHUNK_INFO(pos.x, pos.y, pos.z);
	}
	void Region::SetChunk(Vector3i pos, BlockID* chunkData, unsigned int element_count)
	{
		//NOTE::::::::::::
		//the reason we have to divide the sectorSize by the size of the block ID is due to us passing the element count in, not the size of the block of data.
		//this forces us to make a concession somewhere to keep sectorSize in BYTES NOT ELEMENTS. so we make the concession here for arbitrary reasons.
		int oldSectorCount = GET_CHUNK_INFO(pos.x, pos.y, pos.z).chunkSize / (sectorSize/sizeof(BlockID)) + 1;
		int sectorCount = element_count / (sectorSize / sizeof(BlockID)) + 1;
		if (oldSectorCount != sectorCount)
		{
			//we need to resize the region, and shift things around.
			ResizeAbout(pos, sectorCount, oldSectorCount);
		}
		memcpy(chunks + GET_CHUNK_INFO(pos.x, pos.y, pos.z).chunkOffset / sizeof(BlockID), chunkData, element_count*sizeof(BlockID));
		GET_CHUNK_INFO(pos.x, pos.y, pos.z).chunkSize = element_count;
		isStale = true;
	}
	void Region::ResizeAbout(Vector3i pos, unsigned int newSector, unsigned int oldSector)
	{
		if (newSector > oldSector)
		{
			//just realloc and then shuffle
			memSize += (newSector - oldSector)*sectorSize;
			ResizeMem(memSize);
			ShuffleData(pos, (newSector - oldSector)*sectorSize);
		}
		else
		{
			//we need to shuffle then realloc
			memSize += (newSector - oldSector)*sectorSize;
			ShuffleData(pos, (newSector - oldSector)*sectorSize);
			ResizeMem(memSize);
		}
	}
	bool Region::ResizeMem(unsigned int memSize)
	{
		void* tmp = realloc(block, memSize);
		if (tmp)
		{
			block = tmp;
			chunkStats = (ChunkInfo*)block;
			chunks = (BlockID*)(chunkStats + cubicSector);
			return true;
		}
		return false;
	}
	void Region::ShuffleData(Vector3i pos, unsigned int deltaOffset)
	{
		unsigned int workingPos = GET_ARRAY_POS(pos.x, pos.y, pos.z);
		//set up the header info
		for (unsigned int i = workingPos + 1; i < cubicSector; i++)
		{
			chunkStats[i].chunkOffset += deltaOffset;
		}
		//so the block of memory we need to move starts at our next element e.g. the offset of us +1 element
		//then the remaining block of memory.
		if (workingPos + 2 >= cubicSector)
			return;
		unsigned  oldOffset, newOffset;
		unsigned  moveSize;
		newOffset = chunkStats[workingPos + 1].chunkOffset;
		oldOffset = newOffset - deltaOffset;
		moveSize = chunkStats[cubicSector - 1].chunkOffset + (chunkStats[cubicSector - 1].chunkSize / sectorSize + 1)*sectorSize - newOffset;
		memmove((char*)chunks + newOffset, (char*)chunks + oldOffset, moveSize);
	}
	void Region::PrintHeaderInfo()
	{
		for (int i = 0; i < cubicSector; i++)
		{
			printf("Index:%d\tSize:%d\tOffset:%d\n", i, chunkStats[i].chunkSize, chunkStats[i].chunkOffset);
		}
		printf("End point of the entire block o' data:%d\n", chunkStats[cubicSector - 1].chunkOffset + (chunkStats[cubicSector - 1].chunkSize / sectorSize + 1)*sectorSize);
	}

	bool Region::loadFromDisk()
	{
		string path = "Saves";
		string filename = region_pos.to_string() + ".spawn_region";
		auto data = FileIO::loadFromDisk_nonmanaged<char>(path, filename);
		if (get<1>(data) == 0)
			return false;
		if (block != nullptr)
		{
			free(block);
			chunks = nullptr;
			chunkStats = nullptr;
			block = nullptr;
			memSize = 0;
		}
		memSize = get<1>(data);
		block = get<0>(data);
		chunkStats = (ChunkInfo*)block;
		chunks = (BlockID*)(chunkStats + cubicSector);
		isStale = false;
		return true;
	}
	bool Region::storeToDisk()
	{
		if (!isStale)
			return true;
		string path = "Saves";
		string filename = region_pos.to_string() + ".spawn_region";
		if (!FileIO::storeToDisk<char>(path, filename, (char*)block, memSize, true))
		{
			cout << "Could not write region: " << path + filename << " to disk\n";
			return false;
		}
		isStale = false;
		return true;
	}
}
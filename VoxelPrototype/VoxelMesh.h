#pragma once
#include "VoxelLib.h"
namespace Voxel
{
	//pass this the quad data from a chunk that's being meshed, then fill the vbo with this mesh data.
	//this class determines the amount of data in a quad by checking the constants in VoxelModelData
	class VoxelMesh
	{
	private:
		std::vector<GLfloat> vertices, normals, uvs, colors;
		std::vector<GLuint> indices;
		unsigned int dataPassCount;
	public:
		VoxelMesh();
		~VoxelMesh();
		void addData(VoxelModelData& data);
		//scales the mesh down to it's correct size.
		void fitMesh();
		void pushMesh(std::shared_ptr<Model> model);
		//if passed true, just sets verts/normals/uvs/colors/indices = NULL, else calls free() on them and sets NULL. sets dataPassCount=0
		void clear();
		void Print();
	};
}
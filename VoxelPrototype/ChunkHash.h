#pragma once
#include "VoxelLib.h"
namespace Voxel
{
	//this class will function as a hash table for chunks, specifying the size will create a table size^3
	class ChunkHash
	{
	private:
		int size;
		int tableSize;
		//shared_ptr to array.
		std::shared_ptr<Chunk> table;
	public:
		ChunkHash(int size);
		ChunkHash();
		~ChunkHash();
		//returns the size of the side of the hash table in 3D space (e.g. tableSize = DimSize^3)
		int DimSize() const;
		//returns the maximum number of pointers this hash can store (e.g. tablesize = DimSize^3)
		int TableSize() const;
		//attempts to find the chunk with specified location, if the chunk is valid/found it is returned. else NULL
		Chunk* GetChunk(Vector3i chunkArrayPos);
		Chunk& operator[](Vector3i& ind);
		//mostly used for rendering aka easy iterating over the list of chunks.
		//FOR THE LOVE OF FUCK: DO NOT USE TO MOVE THE WORLD, THIS IS RENDERING ONLY GOD DAMN
		//fun out of bounds errors occur if you don't use it intelligently, instead use the [Vector3i] version, that has bounds checking.
		Chunk& operator[](unsigned int ind);
	};
}
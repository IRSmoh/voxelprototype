#pragma once
#include "VoxelLib.h"
namespace Voxel
{
	//do not delete passed pointer data, this class deletes them itself!
	//stores a single quad from the chunk, and is used to relay data to VoxelMesh.
	class VoxelModelData
	{
	public:
		VoxelModelData();
		~VoxelModelData();
		void addVerts(float* verts);
		void addNormals(float* normals);
		void addUvs(GLfloat* uvs);
		void addColors(GLfloat* colors);
		void addIndices(GLuint* indices);

		GLfloat *vertices, *normals, *uvs, *colors;
		GLuint* indices;
		//4 points of a square, the amount of data that's per section
		const static int vertCount = 4 * 3;
		const static int normalCount = 4 * 3;
		const static int uvCount = 4 * 3;
		const static int colorCount = 4 * 4;
		const static int indexCount = 6;
	};
}
#include "VoxelLib.h"
namespace Voxel
{
	VoxelModelData::VoxelModelData()
	{
		vertices = normals = uvs = colors = NULL;
		indices = NULL;
	}
	VoxelModelData::~VoxelModelData()
	{
		delete[] vertices;
		delete[] normals;
		delete[] uvs;
		delete[] colors;
		delete[] indices;
	}
	void VoxelModelData::addVerts(GLfloat* verts)
	{
		this->vertices = verts;
	}
	void VoxelModelData::addNormals(GLfloat* normals)
	{
		this->normals = normals;
	}
	void VoxelModelData::addUvs(GLfloat* uvs)
	{
		this->uvs = uvs;
	}
	void VoxelModelData::addColors(GLfloat* colors)
	{
		this->colors = colors;
	}
	void VoxelModelData::addIndices(GLuint* indices)
	{
		this->indices = indices;
	}
}
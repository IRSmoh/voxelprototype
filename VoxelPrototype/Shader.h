#ifndef SHADER
#define SHADER
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <stdio.h>
#include <stdlib.h>
//pass this a valid file location for a vertex and fragment shader
//creates the shader on the gpu and links both vert and frag together as a single shader.
//the shader id is stored in programID
class Shader {
public:
	Shader(char* vertexProgram, char* fragmentProgram);
	~Shader();
	GLint programID;
private:
	char* readFile(char* filename);
	bool compiledStatus (GLint shaderID);
	bool linkedStatus (GLint programID);
	GLint makeVertexShader(char* shaderFile);
	GLint makeFragmentShader(char* shaderFile);
	GLint makeShaderProgram(GLint vertexShaderID, GLint fragmentShaderID);
};
#endif
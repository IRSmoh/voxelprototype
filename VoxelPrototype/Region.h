#pragma once
#include "VoxelLib.h"
namespace Voxel
{
	class Region
	{
	public:
		struct ChunkInfo
		{
			//the amount of bytes we occupy, note: a fully uncompressed chunk = max of 65KB
			unsigned short chunkSize;
			//the starting position for this chunk, aka chunk0 is always offset = 0
			//chunk1 would be chunk0 size/sectorSize +1 (since we must have at min 1 sector)
			//note: this is in bytes, not elements.
			unsigned int chunkOffset;
		};
	private:
		ChunkInfo* chunkStats;
		BlockID* chunks;			//the actual starting position for the chunk data.
		void* block;	//our actual data, holds the asd asd asd of the asding d'd of asd.	//holds the prior three things in one block of memory, easy to write to file etc.
		//give us the position we need to shift, and the new/old sectors so we know how to shift the data around. runs in O(n) where n is the number of elements after this slot.
		void ResizeAbout(Vector3i pos, unsigned int newSector, unsigned int oldSector);
		//resizes our block to the appropriate size, if the realloc fails, returns false, else true
		bool ResizeMem(unsigned int memSize);
		//pos is our editing chunk, and offset is the sector size of that chunk (needed for shifting data)
		void ShuffleData(Vector3i pos, unsigned int deltaOffset);
	public:
		Vector3i region_pos;
		const static int sectorSize = 128;	//tweak this in powers of 2 until the size is minimized for chunk expasion/file size. 
		const static int sectorShift = 4;
		const static int sectorDim = 1 << sectorShift;	//single side of the cubic chunk volumn this can contain.
		const static int cubicSector = sectorDim*sectorDim*sectorDim;
		//the size of the header can never change, no reason to try and recalculate it all the time.
		const static unsigned long long headerSize = cubicSector*sizeof(ChunkInfo);
		unsigned int memSize;		//in bytes
		float lastUpdate;			//since regions can be phased out over time, we need a way of keeping track of their last 'check'
		bool isStale;				//same as in Chunk.h, this is meant to indicate if the region needs to be stored to disk or not. e.g. has it been edited?
		Region();
		Region(Vector3i pos);
		~Region();
		Region(Region&) = delete;
		Region operator=(Region&) = delete;
		/*ptr to the compressed chunk*//*size of compressed data*/
		std::tuple<unsigned short*, unsigned short> GetChunk(Vector3i pos);
		ChunkInfo GetChunkInfo(Vector3i pos);
		//pos-> our co-ordinates in the region e.g. Chunk::ArrayPos%sectorDim
		//chunkData is our highly compressed data
		//size is the size in bytes of the compressed data.
		void SetChunk(Vector3i pos, BlockID* chunkData, unsigned int element_count);
		void PrintHeaderInfo();
		//returns true if it successfully loaded a region in, returns false if it failed to load the file.
		//auto destroys the old region data if the file loaded successfully.
		bool loadFromDisk();
		//returns true if the region has been stored or doesn't need to be stored, if it fails to store it returns false.
		bool storeToDisk();
	};
}

#include "Player.h"
#include "BlockCast.h"
#define dim 2
#define rayDist 20
#pragma warning(disable: 4244)
Player::Player()
{
	camera = NULL;
}
Player::Player(Voxel::Map* map, shared_ptr<Camera> camera, Vector3f position)
{

	transform = shared_ptr<Transform>(new Transform(Vector3f(0, 0, 0), Vector3f(1, 1, 1), Vector3f(0, 0, 0)));
	model = NULL;
	this->map = map;
	this->camera = camera;
	transform->position = position;
	camera->transform->parent = this->transform.get();
	transform->parent = map->transform.get();
	currBlock = 1;
}
Player::~Player()
{
}
void Player::Update()
{
	map->manager->LoadNearbyChunks(transform->localPosition());
	if (Keys::ScrollAxis(SCROLL_AXIS_VERTICAL) > 0)
	{
		++currBlock;
		if (currBlock >= Voxel::MapData::blocks.size())
			currBlock = 1;
	}
	else if (Keys::ScrollAxis(SCROLL_AXIS_VERTICAL) < 0)
	{
		--currBlock;
		if (currBlock <= 0)
			currBlock = Voxel::MapData::blocks.size() - 1;
	}
	if (Keys::MouseButtonDown(MOUSE_BUTTON_LEFT))
	{
		bool hit;
		Vector3i hitPos, lastPos;
		Voxel::BlockCast::Cast(transform->localPosition(), transform->localMatrix*(-Vector3f::Forward()), rayDist, hit, hitPos, lastPos, *map, false);
		if (hit)
		{
			map->PlaceBlock(currBlock, lastPos);
		}
	}
	if (Keys::MouseButtonDown(MOUSE_BUTTON_RIGHT))
	{
		bool hit;
		Vector3i hitPos, lastPos;
		Voxel::BlockCast::Cast(transform->localPosition(), transform->localMatrix*(-Vector3f::Forward()), rayDist, hit, hitPos, lastPos, *map, false);
		if (hit)
		{
			map->PlaceBlock(0, hitPos);
		}
	}
	if (Keys::GetKey(KEY_LEFT_SHIFT))
	{
		speed = movespeed*speedMult*Timer::deltaSec();
	}
	else
	{
		speed = movespeed*Timer::deltaSec();
	}
	if (Keys::GetKey(KEY_W))
	{
		transform->Move(Vector3f::Forward()*-speed);
	}
	else if (Keys::GetKey(KEY_S))
	{
		transform->Move(Vector3f::Forward()*speed);
	}
	if (Keys::GetKey(KEY_A))
	{
		transform->Move(Vector3f::Right()*-speed);
	}
	else if (Keys::GetKey(KEY_D))
	{
		transform->Move(Vector3f::Right()*speed);
	}
	if (Keys::GetKey(KEY_LEFT_CONTROL))
	{
		transform->Move(Vector3f::Up()*-speed);
	}
	else if (Keys::GetKey(KEY_SPACE))
	{
		transform->Move(Vector3f::Up() *speed);
	}

	if (!Keys::KeyToggled(KEY_F) && Keys::mouseUpdate)
	{
		Rotate(Keys::deltaX, Keys::deltaY);
		Keys::mouseUpdate = false;
	}

	transform->regenLocalMatrix = true;
	transform->regenWorldMatrix = true;
	transform->UpdateWorldMatrix();
}
void Player::Rotate(float deltaX, float deltaY)
{
	//design a method that allows specifying a pole that the player cannot pass e.g. rotating the camera up/down would prevent movement through the Y axis (for simple cases)
	rotSpeed = baseRotSpeed*Timer::deltaSec();
	if (deltaX > 0)
	{
		transform->eulerAngles.y -= rotSpeed;
	}
	else if (deltaX < 0)
	{
		//rotate right
		transform->eulerAngles.y += rotSpeed;
	}
	if (deltaY >0)
	{
		if (transform->eulerAngles.x - rotSpeed > -PI/2)
			transform->eulerAngles.x -= rotSpeed;
	}
	else if (deltaY < 0)
	{
		if (transform->eulerAngles.x + rotSpeed < PI/2)
			transform->eulerAngles.x += rotSpeed;
	}
	transform->regenLocalMatrix = true;
	transform->regenWorldMatrix = true;
	transform->UpdateWorldMatrix();
}
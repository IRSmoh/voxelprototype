#pragma once
#include "VoxelLib.h"
namespace Voxel
{
	class Chunk;
	class MapData
	{
		//this class will hold the blocks, and the seed for the map and any generation styles aka what noise algo to use, what the initial values are etc.
		//meaning this class should be serializable, editable from the file it creates and editing that should reflect in how the world is built.
	public:
		typedef unsigned long long mapSeed;
		static mapSeed seed;
		static std::vector<Block> blocks;
		static Texture2DArray voxelTexts;
		static void InitVoxelTexts(Shader* shader);

		static void Init(mapSeed seed);
		static void addBlockType(Block block);
		//compairs all stored Block.name in blocks with blockName, if it fails returns -1
		static unsigned int findBlockTypeIndex(std::string& blockName);

		//assign some shitty defaults, eventualy this should be moved to fill and stop being used.
		static void DefaultTest();
	};
}
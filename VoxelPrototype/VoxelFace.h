#pragma once
class VoxelFace
{
public:
	bool transparent;
	int type;
	int side;
	VoxelFace();
	VoxelFace(bool transparent, int type, int side);
	~VoxelFace();
};

static bool operator== (const VoxelFace& left, const VoxelFace& right)
{
	if (left.transparent == right.transparent && left.type == right.type && left.side == right.side)
	{
		return true;
	}
	return false;
}
static bool operator!= (const VoxelFace& left, const VoxelFace& right)
{
	return !(left == right);
}


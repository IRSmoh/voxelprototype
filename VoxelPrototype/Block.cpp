#include "VoxelLib.h"
namespace Voxel
{
	Block::Block(BlockID _blockID, bool _transparent, bool _solid, std::string& _name) : blockID(_blockID), transparent(_transparent), solid(_solid), name(_name)
	{
		for (int i = 0; i < 6; i++)
		{
			textID[i] = 0;
		}
	}
	Block::Block(BlockID _blockID, bool _transparent, bool _solid, const char* _name) : blockID(_blockID), transparent(_transparent), solid(_solid), name(_name)
	{
		for (int i = 0; i < 6; i++)
		{
			textID[i] = 0;
		}
	}
	Block::Block() : blockID(0), transparent(true), name("")
	{
		for (int i = 0; i < 6; i++)
		{
			textID[i] = 0;
		}
	}
	Block::Block(BlockID _blockID, bool _transparent, bool _solid, std::string& _name, textHandle(&textureIndex)[6]) : blockID(_blockID), transparent(_transparent), solid(_solid), name(_name)
	{
		memcpy(textID, textureIndex, sizeof(textID));
	}
	Block::Block(BlockID _blockID, bool _transparent, bool _solid, const char* _name, textHandle(&textureIndex)[6]) : blockID(_blockID), transparent(_transparent), solid(_solid), name(_name)
	{
		memcpy(textID, textureIndex, sizeof(textID));
	}
	Block::Block(BlockID _blockID, bool _transparent, bool _solid, std::string& _name, textHandle south, textHandle north, textHandle east, textHandle west, textHandle top, textHandle bottom) : blockID(_blockID), transparent(_transparent), solid(_solid), name(_name)
	{
		textID[SOUTH] = south;
		textID[NORTH] = north;
		textID[EAST] = east;
		textID[WEST] = west;
		textID[TOP] = top;
		textID[BOTTOM] = bottom;
	}
	Block::Block(BlockID _blockID, bool _transparent, bool _solid, const char* _name, textHandle south, textHandle north, textHandle east, textHandle west, textHandle top, textHandle bottom) : blockID(_blockID), transparent(_transparent), solid(_solid), name(_name)
	{
		textID[SOUTH] = south;
		textID[NORTH] = north;
		textID[EAST] = east;
		textID[WEST] = west;
		textID[TOP] = top;
		textID[BOTTOM] = bottom;
	}
	Block::~Block()
	{
	}
	void Block::setTexture(textHandle textureIndex, unsigned char side)
	{
		if (side >= 0 && side < 6)
		{
			textID[side] = textureIndex;
		}
	}
	void Block::setTexture(textHandle(&textureIndex)[6])
	{
		memcpy(textID, textureIndex, sizeof(textID));
	}
	bool Block::operator==(Block& right)
	{
		if (blockID == right.blockID && transparent == right.transparent)
		{
			return true;
		}
		return false;
	}
	bool Block::operator!=(Block& right)
	{
		return !(*this == right);
	}
}
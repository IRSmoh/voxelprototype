#include "VoxelLib.h"
namespace Voxel
{
#define chunkScale 1.0f
#define GET_BLOCK(ptr, x, y, z) ptr[(x) + (y)*Chunk_Side + (z)*Chunk_Side*Chunk_Side]

#define topColor 1.0f
#define bottomColor 0.45f
#define frontColor 0.8f
#define sideColor 0.54f
#pragma warning(disable: 4244)

	Chunk::Chunk(std::shared_ptr<Shader> shader, Vector3i arrayPos, ChunkManager* manager)
	{
		transform = std::shared_ptr<Transform>(new Transform(Vector3f(0, 0, 0), Vector3f(1, 1, 1), Vector3f(0, 0, 0)));
		model = std::shared_ptr<Model>(new Model(shader, transform));

		this->manager = manager;
		this->arrayPos = arrayPos;
		transform->scale = Vector3f(chunkScale, chunkScale, chunkScale);
		transform->position = Vector3f((float)arrayPos.x, (float)arrayPos.y, (float)arrayPos.z)*Chunk_Side;
		isDirty = false;
		isNew = true;
		loadVBO = false;
		isStale = false;
		tempMesh = shared_ptr<VoxelMesh>(new VoxelMesh());
		model = std::shared_ptr<Model>(new Model(shader, transform));
		model->SetTexture(&MapData::voxelTexts);
		model->uvIndivialSize = 3;
	}
	Chunk::Chunk()
	{
		manager = nullptr;
		isDirty = false;
		isNew = true;
		loadVBO = false;
		isStale = false;
	}
	Chunk::~Chunk()
	{
	}
	void Chunk::BuildChunk()
	{
		//we create the array here due to the array being thrown out frequently/always if it's loaded from disk, vs this will only be called once (or it damn well better be called only once)
		Blocks = std::shared_ptr<BlockID>(new BlockID[chunkSize], Deleter::arrayDelete<BlockID>);
		int x, y, z;
		Vector3i offset;
		BlockID* blockPtr = Blocks.get();
		HeightMap heightMap = manager->heightMap[arrayPos];
		for (z = 0; z < Chunk_Side; z++)
		{
			for (y = 0; y < Chunk_Side; y++)
			{
				for (x = 0; x < Chunk_Side; x++)
				{
					offset = arrayPos*Chunk_Side + Vector3i(x, y, z);
					GET_BLOCK(blockPtr, x, y, z) = WorldGen::genBlock(offset, heightMap[Vector3i(x,y,z)]);
				}
			}
		}
		//we have to let the engine find out the chunk is dirty and remesh, mostly due to the engine checking all chunks in 1 thread vs 12+ threads aka buildChunks in ChunkManager
		isDirty = true;
		isStale = true;
		isNew = false;
	}
	void Chunk::AssignBlocks(std::shared_ptr<BlockID> Blocks)
	{
		this->Blocks = Blocks;
		isDirty = true;
		isNew = false;
		isStale = false;
		DirtyAllNeighbors();
	}
	void Chunk::Render()
	{
		if (loadVBO)
		{
			//push our mesh off, since we have a new one
			tempMesh->pushMesh(model);
			//pass it to the GPU
			model->loadVBO();
			loadVBO = false;
		}
		model->Render();
	}
	void Chunk::BuildMesh()
	{
		isDirty = false;
		tempMesh->clear();
		GreedyMesh();
		loadVBO = true;
	}
	void Chunk::GreedyMesh()
	{
		//scan through Blocks[]
		int i, j, k, l, w, h, u, v, n, side = 0;
		int x[3], q[3], du[3], dv[3];
		//this mask is used to pullout the offset from 0, wrapping on the 32 boundary, e.g. -1 becomes 31
		BlockID mask[Chunk_Side*Chunk_Side];
		//create temporaries for comparing faces
		BlockID voxel, voxel1;
		BlockID* blockPtr = Blocks.get();
		bool done;
		//we need to grab our neighbors so we can mes
		Chunk** neighborChunks = FetchNeighbors();
		//the variable backFace will be true on the first iteration, and false on the second.. which allows us to track the direction the indices should run during creation of the quad(s)
		//runs 6x total 2 for outer loop, 3 for inner loop total run time chunk_Side^6 = ~16.8 million checks assuming chunk_side = 16

		//check to make sure the block we are meshing isn't obscured by the block infront. if(backface) check next layer at same co-ords

		for (bool backFace = true, b = false; b != backFace; backFace = backFace && b, b = !b)
		{
			//go over the 3 dimensions of the chunk
			for (int d = 0; d < 3; d++)
			{
				u = (d + 1) % 3;
				v = (d + 2) % 3;

				x[0] = x[1] = x[2] = 0;
				q[0] = q[1] = q[2] = 0;
				q[d] = 1;
				//keep track of the side we are meshing
				if (d == 0) { side = backFace ? WEST : EAST; }
				else if (d == 1) { side = backFace ? BOTTOM : TOP; }
				else if (d == 2) { side = backFace ? SOUTH : NORTH; }
				//move through the dimension from front to back
				for (x[d] = -1; x[d] < Chunk_Side;)
				{
					//compute the mask
					n = 0;
					for (x[v] = 0; x[v] < Chunk_Side; x[v]++)
					{
						for (x[u] = 0; x[u] < Chunk_Side; x[u]++)
						{
							//if the access would normally be negative/out of bounds, then wrap to the neighbor chunk if not null.
							//
							//retrieve ourself
							if (x[d] < 0)
							{
								if (neighborChunks[side] && backFace)
								{
									voxel = (*neighborChunks[side])[Vector3i(x[0], x[1], x[2]) & Chunk_BitMask];
								}
								else
								{
									voxel = Air_Block;
								}
							}
							else
							{
								voxel = GET_BLOCK(blockPtr, x[0], x[1], x[2]);
							}
							if (x[d] == Chunk_Side - 1)
							{
								if (neighborChunks[side] && !backFace)
								{
									voxel1 = (*neighborChunks[side])[Vector3i(x[0] + q[0], x[1] + q[1], x[2] + q[2]) & Chunk_BitMask];
								}
								else
								{
									voxel1 = Air_Block;
								}
							}
							else
							{
								voxel1 = GET_BLOCK(blockPtr, x[0] + q[0], x[1] + q[1], x[2] + q[2]);
							}
							//voxel = x[d] >= 0 ? GET_BLOCK(x[0], x[1], x[2]) : Air_Block;
							//retrieve the voxel that can be obscuring us. e.g. infront of us.
							//voxel1 = x[d] < (Chunk_Side - 1) ? GET_BLOCK(x[0] + q[0], x[1] + q[1], x[2] + q[2]) : Air_Block;
							//this if statement dictates what blocks are actually going to get meshed.
							//if either block is air, ignore it
							//if both blocks are not transparent !(transparent | transparent) then ignore their layers.
							//if the blocks are transparent, but they are the same, ignore them
							//if the blocks are transparent, and different consider them (aka mesh)
							if (voxel != Air_Block && voxel1 != Air_Block && ((voxel == voxel1) || !(MapData::blocks[voxel].transparent || MapData::blocks[voxel1].transparent)))
							{
								mask[n++] = Air_Block;
							}
							else
							{
								mask[n++] = backFace ? voxel1 : voxel;
							}
						}
					}
					x[d]++;
					//generate the mesh for the mask

					n = 0;

					for (j = 0; j < Chunk_Side; j++)
					{
						for (i = 0; i < Chunk_Side;)
						{
							if (mask[n] != Air_Block)
							{
								//compute the width
								for (w = 1; i + w < Chunk_Side && mask[n + w] != Air_Block && mask[n + w] == mask[n]; w++){}
								//compute the height
								done = false;
								for (h = 1; j + h < Chunk_Side; h++)
								{
									for (k = 0; k < w; k++)
									{
										if (mask[n + k + h*Chunk_Side] == Air_Block || mask[n + k + h*Chunk_Side] != mask[n]) { done = true; break; }
									}
									if (done){ break; }
								}
								//check the transparent attribute to make sure its not needed for meshing (what about water you idiots?)
								//make the quad
								x[u] = i;
								x[v] = j;

								du[0] = du[1] = du[2] = 0;
								du[u] = w;

								dv[0] = dv[1] = dv[2] = 0;
								dv[v] = h;
								//pass it to the quad
								Quad(Vector3f(x[0], x[1], x[2]),
									Vector3f(x[0] + du[0], x[1] + du[1], x[2] + du[2]),
									Vector3f(x[0] + du[0] + dv[0], x[1] + du[1] + dv[1], x[2] + du[2] + dv[2]),
									Vector3f(x[0] + dv[0], x[1] + dv[1], x[2] + dv[2]),
									w, h, mask[n], backFace, side);
								//zero the mask
								for (l = 0; l < h; ++l)
								{
									for (k = 0; k < w; ++k){ mask[n + k + l*Chunk_Side] = Air_Block; }
								}
								//increment and continue
								i += w;
								n += w;
							}
							else
							{
								++i;
								++n;
							}
						}
					}
				}
			}
		}
		delete[] neighborChunks;
	}
	void Chunk::Quad(Vector3f& bottomLeft, Vector3f& topLeft, Vector3f& topRight, Vector3f& bottomRight, int width, int height, BlockID voxel, bool backFace, int side)
	{
		//this is being left because it's far easier to read this way.
		//vertices[2] = topLeft;
		//vertices[3] = topRight;
		//vertices[0] = bottomLeft;
		//vertices[1] = bottomRight;
		GLfloat* vertices = new GLfloat[4 * 3]{bottomLeft.x, bottomLeft.y, bottomLeft.z,
			bottomRight.x, bottomRight.y, bottomRight.z,
			topLeft.x, topLeft.y, topLeft.z,
			topRight.x, topRight.y, topRight.z};

		//figure out what order they're supposed to be in
		GLuint* indices;
		if (backFace)
		{
			indices = new GLuint[6]{ 2, 0, 1, 1, 3, 2 };
		}
		else
		{
			indices = new GLuint[6]{ 2, 3, 1, 1, 0, 2 };
		}
		float textID = MapData::blocks[voxel].textID[side];
		GLfloat*uvs;
		if (side == NORTH || side == SOUTH)
		{
			uvs = new GLfloat[12]{(float)width, (float)height, textID,
				(float)width, 0, textID,
				0, (float)height, textID,
				0, 0, textID};
		}
		else
		{
			uvs = new GLfloat[12]{0, (float)width, textID,
				(float)height, (float)width, textID,
				0, 0, textID,
				(float)height, 0, textID};
		}
		float currColor;
		if (side == NORTH || side == SOUTH)
		{
			currColor = frontColor;
		}
		else if (side == EAST || side == WEST)
		{
			currColor = sideColor;
		}
		else if (side == TOP)
		{
			currColor = topColor;
		}
		else
		{
			currColor = bottomColor;
		}
		GLfloat* colors = new GLfloat[16]{currColor, currColor, currColor, 1,
			currColor, currColor, currColor, 1,
			currColor, currColor, currColor, 1,
			currColor, currColor, currColor, 1};
		//pass it off to the voxel data.
		VoxelModelData data;
		data.addUvs(uvs);
		data.addIndices(indices);
		data.addVerts(vertices);
		data.addColors(colors);
		//pass it off to the mesh in control of rendering this fucker.
		tempMesh->addData(data);
	}
	void Chunk::PlaceBlock(BlockID block, Vector3i pos)
	{
		GET_BLOCK(Blocks.get(), pos.x, pos.y, pos.z) = block;
		isDirty = true;
		isStale = true;
		DirtyNeighbors(pos);
	}
	//operators
	BlockID& Chunk::operator[](Vector3i index)
	{
		//in secondary note: do not pass by reference the index, for some reason its faster not to.
		return GET_BLOCK(Blocks.get(), index.x, index.y, index.z);
	}
	Chunk** Chunk::FetchNeighbors()
	{
		//grab the 6 neighboring chunks,
		//the directions and their indications:
		//order: SOUTH, NORTH, EAST, WEST, TOP, BOTTOM
		//north = +z
		//south = -z
		//east = +x
		//west = -x
		//top = +y
		//bottom/down = -y
		Chunk** neighborChunks =new Chunk*[numNeighborChunks];
		if (manager)
		{
			Vector3i dims[]{Vector3i::Forward(), Vector3i::Back(),
				Vector3i::Right(), Vector3i::Left(),
				Vector3i::Up(), Vector3i::Down()};
			for (int i = 0; i < numNeighborChunks; i++)
			{
				neighborChunks[i] = manager->Chunks.GetChunk(arrayPos + dims[i]);
			}
		}
		else
		{
			for (int i = 0; i < numNeighborChunks; i++)
			{
				neighborChunks[i] = nullptr;
			}
		}
		return neighborChunks;
	}
	void Chunk::DirtyNeighbors(Vector3i pos)
	{
		Chunk** neighborChunks = FetchNeighbors();
		if (pos.x == 0)
		{
			if (neighborChunks[WEST])
				neighborChunks[WEST]->isDirty = true;
		}
		else if (pos.x == Chunk_Side - 1)
		{
			if (neighborChunks[EAST])
				neighborChunks[EAST]->isDirty = true;
		}
		if (pos.y == 0)
		{
			if (neighborChunks[BOTTOM])
				neighborChunks[BOTTOM]->isDirty = true;
		}
		else if (pos.y == Chunk_Side - 1)
		{
			if (neighborChunks[TOP])
				neighborChunks[TOP]->isDirty = true;
		}
		if (pos.z == 0)
		{
			if (neighborChunks[SOUTH])
				neighborChunks[SOUTH]->isDirty = true;
		}
		else if (pos.z == Chunk_Side - 1)
		{
			if (neighborChunks[NORTH])
				neighborChunks[NORTH]->isDirty = true;
		}
		delete[] neighborChunks;
	}
	void Chunk::DirtyAllNeighbors()
	{
		Chunk** neighborChunks = FetchNeighbors();
		for (int i = 0; i < numNeighborChunks; i++)
		{
			if (neighborChunks[i])
				neighborChunks[i]->isDirty = true;
		}
		delete[] neighborChunks;
	}
}

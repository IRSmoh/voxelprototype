#pragma once
#include <chrono>
using namespace std;
class Timer
{
public:
	//holy tits thats a long declare

	static chrono::time_point<chrono::system_clock, chrono::system_clock::duration> startTime;
	static chrono::duration<chrono::system_clock::rep, chrono::system_clock::period> endTime;
	//call to zero out the timer.
	static void InitTime();
	//call at the begining of the main game loop and once elsewhere to populate with reasonable values.
	static void ForwardTimer();
	//request elapsed time since last call to ForwardTimer()
	static double deltaMicroSec();
	//request elapsed time since last call to ForwardTimer()
	static double deltaMilliSec();
	//request elapsed time since last call to ForwardTimer()
	static double deltaSec();
};



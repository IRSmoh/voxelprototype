#include "CubeMap.h"


CubeMap::CubeMap()
{
}


CubeMap::~CubeMap()
{
}

CubeMap::CubeMap(string& topFile, string& bottomFile, string& leftFile, string& rightFile, string& backFile, string& frontFile, int channelType)
{
	//this isn't really doing error handling... like anywhere.. that should be fixed probably.
	error = lodepng::decode(top, width, height, topFile.c_str());

	error = lodepng::decode(bottom, width, height, bottomFile.c_str());

	error = lodepng::decode(left, width, height, leftFile.c_str());

	error = lodepng::decode(right, width, height, rightFile.c_str());

	error = lodepng::decode(back, width, height, backFile.c_str());

	error = lodepng::decode(front, width, height, frontFile.c_str());
	this->channelType = channelType;
}
CubeMap::CubeMap(string& file, int channelType)
{
	this->channelType = channelType;
	error = lodepng::decode(image, width, height, file.c_str());
	ParseFile();
	image.clear();
	image.shrink_to_fit();
}
void CubeMap::PushTexture(GLint shaderID)
{
	glEnable(GL_TEXTURE_CUBE_MAP);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
	//push the right texture
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, channelType, width, height, 0, channelType, GL_UNSIGNED_BYTE, &right[0]);
	//destroy the data since we don't need it due to being on the gpu
	right.clear();
	right.shrink_to_fit();
	//push the left texture
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, channelType, width, height, 0, channelType, GL_UNSIGNED_BYTE, &left[0]);
	left.clear();
	left.shrink_to_fit();
	//push the front texture
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, channelType, width, height, 0, channelType, GL_UNSIGNED_BYTE, &front[0]);
	front.clear();
	front.shrink_to_fit();
	//push the back texture
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, channelType, width, height, 0, channelType, GL_UNSIGNED_BYTE, &back[0]);
	back.clear();
	back.shrink_to_fit();
	//push the up texture
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, channelType, width, height, 0, channelType, GL_UNSIGNED_BYTE, &top[0]);
	top.clear();
	top.shrink_to_fit();
	//push the down texture
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, channelType, width, height, 0, channelType, GL_UNSIGNED_BYTE, &bottom[0]);
	bottom.clear();
	bottom.shrink_to_fit();
	//setup our filters e.g. no mipmapping
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//clamp everything to edges for reasons
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	//grab the position of the cubesampler
	cubemapID = glGetUniformLocation(shaderID, "cubeMap");
}

void CubeMap::copySubPic(pixelData& image, int imageWidth, int minHeight, int maxHeight, int minWidth, int maxWidth, pixelData& result, int channelCount)
{
	int dim = maxHeight - minHeight;
	result = pixelData(dim * dim*channelCount, 0);
	for (int x = 0; x < dim; ++x)
	{
		for (int y = 0; y < dim; ++y)
		{
			for (int i = 0; i < channelCount; i++)
			{
				result[(x)*channelCount + (y)*dim*channelCount + i] = image[(x + minWidth)*channelCount + (y + minHeight)*imageWidth*channelCount + i];
			}
		}
	}
}
void CubeMap::ParseFile()
{
	//determine if the picture is horizontal or not. Why am I describing this I don't even.
	bool horizontal = width > height;
	int channelCount = channelType == GL_RGBA ? 4 : 3;
	if (horizontal)
	{
		/*
		//if the ascii art is broken, rip
		image format:
		<-------------------width------------------->
		---------------------------------------------	^
		|		|			|			|			|	h
		| empty	|	top		|	empty	|	empty	|	e
		---------------------------------------------	i
		|		|			|			|			|	g
		| left	|	front	|	right	|	back	|	h
		---------------------------------------------	t
		|		|			|			|			|	|
		| empty	|	bottom	|	empty	|	empty	|	|
		---------------------------------------------	V
		*/
		//horizontal
		//fill in the top
		copySubPic(image, width, 0, height / 3, width / 4, width / 2, top, channelCount);
		//fill in the left
		copySubPic(image, width, height / 3, 2 * height / 3, 0, width / 4, left, channelCount);
		//fill in the front
		copySubPic(image, width, height / 3, 2 * height / 3, width / 4, width / 2, back, channelCount);
		//fill int the right
		copySubPic(image, width, height / 3, 2 * height / 3, width / 2, 3 * width / 4, right, channelCount);
		//fill in the back
		copySubPic(image, width, height / 3, 2 * height / 3, 3 * width / 4, width, front, channelCount);
		//fill in the bottom
		copySubPic(image, width, 2 * height / 3, height, width / 4, width / 2, bottom, channelCount);
		width = width / 4;
		height = height / 3;
	}
	else
	{
		//vertical
		/*
		//if the ascii art is broken, rip
		<-------width----------->
		-------------------------	^
		|		|		|		|	|
		| empty	| top	| empty	|	h
		-------------------------	e
		|		|		|		|	i
		| left	| front | right |	g
		-------------------------	h
		|		|		|		|	t
		| empty | bottom| empty	|	|
		-------------------------	|
		|		|		|		|	|
		| empty	| back	| empty	|	|
		-------------------------	V
		*/
		//fill in the top
		copySubPic(image, width, 0, height / 4, width / 3, 2 * width / 3, top, 4);
		//fill in the left
		copySubPic(image, width, height / 4, height / 2, 0, width / 3, left, 4);
		//fill in the front
		copySubPic(image, width, height / 4, height / 2, width / 3, 2 * width / 3, front, 4);
		//fill in the right
		copySubPic(image, width, height / 4, height / 2, 2 * width / 3, width, right, 4);
		//fill in the bottom
		copySubPic(image, width, height / 2, 3 * height / 4, width / 3, 2 * width / 3, bottom, 4);
		//fill in the back
		copySubPic(image, width, 3 * height / 4, height, width / 3, 2 * width / 3, back, 4);
		width = width / 3;
		height = height / 4;
	}
}
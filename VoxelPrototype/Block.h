#pragma once
#include "VoxelLib.h"
namespace Voxel
{
	//stores information about basic voxel stats, and contains the various textureIDs or equiv for creating a valid mesh.
	class Block
	{
	public:
		typedef unsigned short textHandle;
		//Use this class as a set of generics for what a block is, leave the location data to how its stored, e.g. 1 variety of the class should only be loaded once.
		std::string name;
		bool transparent;
		bool solid;
		//the ID of this block, aka its position in the main block vector. this way we can have multiple sub copies in various structs/etc
		BlockID blockID;
		//for now this will dictate the layer of the 2dtext array
		//later it will be a gluint64 for the address of the texture when we switch to bindless
		textHandle textID[6];
		//texture
		//properties
		Block(BlockID blockID, bool transparent, bool solid, std::string& name);
		Block(BlockID blockID, bool transparent, bool solid, const char* name);
		Block(BlockID blockID, bool transparent, bool solid, std::string& name, textHandle(&textureIndex)[6]);
		Block(BlockID blockID, bool transparent, bool solid, const char* name, textHandle(&textureIndex)[6]);
		Block(BlockID blockID, bool transparent, bool solid, std::string& name, textHandle south, textHandle north, textHandle east, textHandle west, textHandle top, textHandle bottom);
		Block(BlockID blockID, bool transparent, bool solid, const char* name, textHandle south, textHandle north, textHandle east, textHandle west, textHandle top, textHandle bottom);
		Block();
		virtual ~Block();

		void setTexture(textHandle textureIndex, unsigned char side);
		/*order is...
		south = 0
		north = 1
		east = 2
		west = 3
		top = 4
		bottom = 5
		unfortunately c++ doesn't allow us to pass {0,1,2,3,4,5} as an initializer to this
		*/
		void setTexture(textHandle(&textureIndex)[6]);
		virtual bool operator==(Block& right);
		virtual bool operator!=(Block& right);
	};
}
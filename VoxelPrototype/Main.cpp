#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "btBulletDynamicsCommon.h"
#include "Light.h"
#include <iostream>
#include "VoxelLib.h"
#include "Keys.h"
#include "Player.h"
#include "Timer.h"
#include "SkyBox.h"
#include "FileIO.h"


shared_ptr<Camera> camera;
Player* player;

btBroadphaseInterface* broadphase;
btDefaultCollisionConfiguration* collisionConfiguration;
btCollisionDispatcher* dispatcher;
btSequentialImpulseConstraintSolver* solver;
btDiscreteDynamicsWorld* dynamicsWorld;



int width = 800;
int height = 600;
double oldMouseX = width / 2;
double oldMouseY = height / 2;
double deltaX, deltaY = 0;
float scale = 0.25f;

#define deadzone 20

shared_ptr<Light> lightPtr;
Voxel::Map* map;
int mapDim = 6;
clock_t timer;
double seconds;
shared_ptr<Shader> shader;
shared_ptr<Shader> skyboxShader;

SkyBox skyBox;
CubeMap cubemap;

void Update()
{
	//this must exist to set a check bit for transform (they need to confirm if its a different frame or not)
	Transform::toggleBool = !Transform::toggleBool;
	player->Update();
	map->Update();
}
void Render(GLFWwindow* window)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	camera->setView();
	skyBox.Render();
	map->Render();
	glfwSwapBuffers(window);
}
//so that resizing causes the window to be redrawn
//does not fix holding the window preventing rendering
void window_refresh(GLFWwindow* window)
{
	Render(window);
}
void window_resize(GLFWwindow* window, int width, int height)
{
	camera->perspective = Matrix4x4::makePerspective(70, width / (float)height, 0.5f, 1000.0f);
	glViewport(0, 0, width, height);
}

void PhysicsTest()
{
}
void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}
int main(int argc, char** argv) 
{
	GLFWwindow* window;
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		exit(EXIT_FAILURE);

	GLFWmonitor* monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);
	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

	window = glfwCreateWindow(width, height, "VoxelPrototype", NULL, NULL);

	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(window);
	int glewErr = glewInit();
	if (glewErr != GLEW_OK)
	{
		exit(-1);
	}
	glClearColor(1.0f, 1.0f, 0.9f, 1.0f);


	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	//0 for unlimited fps
	//1 for vsync for current monitor (7ms -> ~144fps & 16.66666ms -> 60fps)
	//wtb understanding how to set it to current refresh rate of monitor...
	glfwSwapInterval(1);

	// Set up the camera
	camera = shared_ptr<Camera>(new Camera(60.0f, (float)width / (float)height, 0.1f, 500.0f));

	//*******************************
	//		  CHUNK SHADER
	//*******************************
	char* chunkVert = "Shaders\\ChunkVert.vsh";
	char* chunkFrag = "Shaders\\ChunkFrag.fsh";
	int err = glGetError();
	shader = shared_ptr<Shader>(new Shader(chunkVert, chunkFrag));
	err = glGetError();
	//*******************************

	//we need to setup all cameras perspective/view matrices. e.g. all cameras will dump to this spot
	Camera::initCameraData(shader->programID);
	//make the voxels textures
	Voxel::MapData::InitVoxelTexts(shader.get());

	//*******************************
	//			SKYBOX
	//*******************************
	char* skyVert = "Shaders\\SkyboxVert.vsh";
	char* skyFrag = "Shaders\\SkyboxFrag.fsh";

	skyboxShader = shared_ptr<Shader>(new Shader(skyVert, skyFrag));

	string picture = "Textures\\skybox.png";
	cubemap = CubeMap(picture, GL_RGBA);
	cubemap.PushTexture(skyboxShader->programID);

	skyBox = SkyBox(skyboxShader);
	skyBox.Init();
	skyBox.SetCubeMap(cubemap);
	//********************************

	//****************************************************
	//					ENGINE		TESTS				 |
	//					  Create player					 |
	//____________________________________________________
				//Start the engine
				//Engine engine;
				//ENGINE->StartUp();
				//ENGINE->EngineTests();
				//ENGINE->GameLoop();

	//_____________________________________________________
	//													  |
	//*****************************************************



	//***********************
	//		Physics
	broadphase = new btDbvtBroadphase();
	collisionConfiguration = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(collisionConfiguration);
	solver = new btSequentialImpulseConstraintSolver;
	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);

	dynamicsWorld->setGravity(btVector3(0, -10, 0));
	//************************

	time_t now;
	time(&now);
	Voxel::MapData::Init(now);

	map = new Voxel::Map(shader, mapDim);
	player = new Player(map, camera, Vector3f(0,256,0));
	Timer::InitTime();
	Timer::ForwardTimer();

	Keys::Init(width,height);
	//callbacks
	glfwSetKeyCallback(window, Keys::KeyboardCallB);
	glfwSetWindowSizeCallback(window, window_resize);
	glfwSetWindowRefreshCallback(window, window_refresh);
	glfwSetCursorPosCallback(window, Keys::MouseMovementCallB);
	glfwSetScrollCallback(window, Keys::SetScrollEvent);
	//Engine camera callbacks:
	//glfwSetCursorPosCallback(window, cam->MouseMovement);						
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	//main game loop
	while (!glfwWindowShouldClose(window))
	{
		Timer::ForwardTimer();
		Update();
		Render(window);
		glfwPollEvents();
		//due to needing exact timings for pressing/releasing the key, we have to manually update the keys once per frame
		//we cannot rely on the callbacks
		Keys::UpdateKeys(window);
	}

	//kill everything
	glfwDestroyWindow(window);
	glfwTerminate();
	delete(player);
	delete(map);
	delete(broadphase);
	delete(collisionConfiguration);
	delete(dispatcher);
	delete(solver);
	delete(dynamicsWorld);
	//give the threads time to destruct & fore the regions to be stored to disk.
	std::this_thread::sleep_for(chrono::milliseconds(200));
	exit(EXIT_SUCCESS);
}
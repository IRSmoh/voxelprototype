#include "VoxelLib.h"
#define GET_HEIGHT(ptr, x,z) ptr[(x)+(z)*Map_Side]
namespace Voxel
{
	int HeightMap::Map_Side = Chunk::Chunk_Side;
	int HeightMap::Map_Size = HeightMap::Map_Side*HeightMap::Map_Side;
	HeightMap::HeightMap()
	{
		built = false;
	}
	HeightMap::HeightMap(Vector3i _gridPos) : gridPos(_gridPos)
	{
		height_map = shared_ptr<float>(new float[Map_Size], Deleter::arrayDelete<float>);
		built = false;
	}
	HeightMap::~HeightMap()
	{
	}
	void HeightMap::Init()
	{
		int x, z;
		float* mapPtr = height_map.get();
		Vector3i worldGrid = gridPos*Map_Side;	//so we don't have to multiply as much.
		//the loop is arranged this way due to cache, since x vals are nearer each other than z vals, we should increment x 'more often'
		for (z = 0; z < Map_Side; z++)
		{
			for (x = 0; x < Map_Side; x++)
			{
				GET_HEIGHT(mapPtr, x, z) = WorldGen::genHeight(worldGrid.x + x, worldGrid.z + z);
			}
		}
		built = true;
	}
	float& HeightMap::operator[](Vector3i pos)
	{
		//pos = pos.preserveOffset(Map_Side);
		return GET_HEIGHT(height_map.get(), pos.x, pos.z);
	}
}
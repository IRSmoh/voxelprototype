#pragma once
#include "VoxelLib.h"
namespace Voxel
{
	//updates the chunks/gives easy access to world based voxel editing
	class Map
	{
	public:
		int z = 0;
		std::shared_ptr<Transform> transform;
		std::shared_ptr<ChunkManager> manager;
		Map();
		Map(std::shared_ptr<Shader> shader, int mapDim);
		~Map();
		virtual void Render();
		virtual void Update();

		void PlaceBlock(BlockID block, Vector3i pos);
		//find the block at the specified location
		void GetBlock(Vector3i pos, bool& found, BlockID& block);
		//does proper casting of the float to preserve position.
		//void GetBlock(Vector3f pos, bool& found, Block::BlockID& block);
	};
}
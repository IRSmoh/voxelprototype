#pragma once
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <vector>
#include "Transform.h"
#include "Texture.h"
#include "Shader.h"
#include <memory>
using namespace std;
//this class is meant to organize all things needed to render an object/mesh on the screen
//this means it has to have a valid index buffer/texture/shader tags (position/normal/color/etc)
//this will require some form of mild linking between a transform instance.
//hopefully will be able to be batched easily
class Model
{
protected:
	vector<GLfloat> vertices;		//the positions for 1/6th of a model (or all of it if you are using this class differently...)
	vector<GLfloat> normals;		//either all of the normals, or if the bool below is true, just one normal (which is global to this entire mesh)
	vector<GLfloat> uvs;			//the texture co-ords
	vector<GLfloat> colors;		//probably going to be unused, but would contain the color data.
	vector<GLuint> indices;		//how to format the cluster fuck of above data.
	GLuint vao;				//the vertex array
	GLuint vbo;				//the vertex buffer
	GLuint index_vbo;		//the index buffer
	GLuint vertexSize, indexSize;

	GLint vPosition;
	GLint vNormal;
	GLint vTexCoord;
	GLint vColor;
	GLint umM;				//reference to the model matrix in the shader

	Texture* text;
public:

	shared_ptr<Transform> transform;			//temp stuff

	unsigned char uvIndivialSize;

	static GLuint lastShader;
	//if true causes the class to destruct by using free() instead of delete[]
	//the programID the shader will be using, to cut down on indirections we aren't storing the full "shader" get by Shader->programID
	GLuint shader;
	bool wireframe;
	//this is just for array initialization, use one of the proper overloads.
	Model();
	Model(shared_ptr<Shader> shader);
	Model(shared_ptr<Shader> shader, shared_ptr<Transform> transform);
	virtual ~Model();
	virtual void setGeometry(vector<GLfloat>& vertices, unsigned int size);
	virtual void setNormal(vector<GLfloat>& normals, unsigned int size);
	virtual void setVertexColors(vector<GLfloat>& colors, unsigned int size);
	virtual void setTextureCoordinates(vector<GLfloat>& uvs, unsigned int size);
	virtual void setIndexBuffer(vector<GLuint>& indices, unsigned int size);
	void loadVBO();
	virtual void Render();
	virtual void SetTexture(Texture* text);
};

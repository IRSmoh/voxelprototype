#pragma once
#include "VoxelLib.h"
namespace Voxel
{
	class HeightMapHash
	{
	private:
		shared_ptr<HeightMap> mapHash;
	public:
		int dim;
		HeightMapHash();
		HeightMapHash(int dim);
		~HeightMapHash();
		//this will only generate new height maps for positions not filled in x or z, if there are copies in y, it ignores generating a new map.
		void GenMap(Vector3i pos);
		HeightMap& operator[](Vector3i pos);
	};
}
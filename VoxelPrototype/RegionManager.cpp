#include "VoxelLib.h"
namespace Voxel
{
#define GET_ACTIVE_REGION(x, y, z) active_regions[((x)%regionDim + regionDim) % regionDim + (((y)%regionDim + regionDim) % regionDim)*regionDim + (((z)%regionDim + regionDim) % regionDim)*regionDim*regionDim]
#define GET_ACTIVE_REGION_POS(pos) active_regions[((pos.x)%regionDim + regionDim) % regionDim + (((pos.y)%regionDim + regionDim) % regionDim)*regionDim + (((pos.z)%regionDim + regionDim) % regionDim)*regionDim*regionDim]
	float RegionManager::Region_Sleep_Delay = 10.0f;
	float RegionManager::Stash_Delay = 10.0f;
	
	RegionManager::RegionManager()
	{
	}
	RegionManager::RegionManager(int loadedChunkDim)
	{
		int offset = loadedChunkDim - 1;
		int div = offset / loadedChunkDim;
		bool overflow = offset % Region::sectorDim != 0;
		regionDim = 1 + div + (overflow ? 1 : 0);
		active_regions.resize(regionDim*regionDim*regionDim);
		for (int x = 0; x < regionDim; x++)
		{
			for (int y = 0; y < regionDim; y++)
			{
				for (int z = 0; z < regionDim; z++)
				{
					GET_ACTIVE_REGION(x, y, z) = nullptr;
				}
			}
		}
		lastStoreTime = 0;
	}
	RegionManager::~RegionManager()
	{
		//store our regions so we don't lose them on destruction of the manager.
		for (int i = 0; i < sleeping_regions.size(); i++)
		{
			sleeping_regions[i]->storeToDisk();
		}
		for (int i = 0; i < active_regions.size(); i++)
		{
			if (active_regions[i] != nullptr)
				active_regions[i]->storeToDisk();
		}
	}
	void RegionManager::Update(float deltaTime)
	{
		lastStoreTime += deltaTime;
		if (lastStoreTime > Stash_Delay)
		{
			lastStoreTime = 0;	//'but we lose some precision!' honestly, its not going to matter.
			for (int i = 0; i < active_regions.size(); i++)
			{
				if (active_regions[i] != nullptr)
					active_regions[i]->storeToDisk();
			}
		}
		//the only thing we need to do is make sure we don't need to keep our regions in memory.
		for (int i = sleeping_regions.size()-1; i >= 0; i--)
		{
			sleeping_regions[i]->lastUpdate += deltaTime;
			//if they have exceeded their time limit, throw them out.
			if (sleeping_regions[i]->lastUpdate > Region_Sleep_Delay)
			{
				sleeping_regions[i]->storeToDisk();
				sleeping_regions.erase(sleeping_regions.begin() + i);
			}
		}
	}
	void RegionManager::StoreChunk(Chunk& chunk)
	{
		Vector3i region_pos = chunk.arrayPos >> Region::sectorShift;
		//trust no operator precendence
		Vector3i internal_pos = chunk.arrayPos & ((1 << Region::sectorShift) - 1);
		//find our region with the region_pos.
		//compress, then store the chunk data into the region based on the internal_pos.
		auto region = GET_ACTIVE_REGION_POS(region_pos);
		if (region == nullptr || region->region_pos != region_pos)
		{
			//if its null, don't bother to do anything fancy other than making a new region and storing it.
			if (region.get() == nullptr)
			{
				GET_ACTIVE_REGION_POS(region_pos) = shared_ptr<Region>(new Region(region_pos));
				GET_ACTIVE_REGION_POS(region_pos)->loadFromDisk();
			}
			//if its not null, store the region into the waiting list ver, scan through the waiting list to see if our region is in there, if not, make it.
			else
			{
				//determine if we have the region still in memory.
				shared_ptr<Region> tmp = nullptr;
				for (int i = 0; i < sleeping_regions.size(); i++)
				{
					if (sleeping_regions[i]->region_pos == region_pos)
					{
						tmp = sleeping_regions[i];
						//we found the correct region, remove it from the vector for double deletion/size reasons.
						sleeping_regions.erase(sleeping_regions.begin() + i);
						break;
					}
				}
				//store our now sleeping region
				sleeping_regions.push_back(GET_ACTIVE_REGION_POS(region_pos));
				//either make a new region if we couldn't find the old region, or place the sleeping_region in here.
				if (tmp == nullptr)
				{
					GET_ACTIVE_REGION_POS(region_pos) = shared_ptr<Region>(new Region(region_pos));
					GET_ACTIVE_REGION_POS(region_pos)->loadFromDisk();
				}
				else
				{
					GET_ACTIVE_REGION_POS(region_pos) = tmp;
				}
			}
			region = GET_ACTIVE_REGION_POS(region_pos);
		}
		//now we can guarantee we have a valid region, compress the chunk and store it to the region.
		auto compressedData = compress(chunk.Blocks.get(), Chunk::chunkSize);
		if (get<0>(compressedData) != nullptr)
			//we have compressed data, store it
			region->SetChunk(internal_pos, get<0>(compressedData).get(), get<1>(compressedData));
		else
			//we couldn't compress, store the raw version instead.
			region->SetChunk(internal_pos, chunk.Blocks.get(), Chunk::chunkSize);
	}
	std::shared_ptr<BlockID> RegionManager::GetChunkData(Vector3i chunk_world_pos)
	{
		Vector3i region_pos = chunk_world_pos >> Region::sectorShift;
		//trust no operator precendence
		//I'm making a note here due to screwing this up twice already.
		//we need to get the masked bits, thus the 'easy' way to do that is to get the pow2 of sector size; then subtract 1 to get a valid mask.
		//e.g. sectorShift = 5, and we want to get those 5 bits (5-1) we shift to the left 5x and get 0x20 (32) (1000) then sub 1 -> 0x1F (31) (01111);
		//this allows us to turn a -1 into 31 and become a valid accessor. alternatively 33 becomes 1, -32 becomes 2. etc and so forth.
		Vector3i internal_pos = chunk_world_pos & ((1 << Region::sectorShift) - 1);
		//find our region with the region_pos.
		auto region = GET_ACTIVE_REGION_POS(region_pos);
		//make sure we have a region for this..
		if (region != nullptr && region->region_pos == region_pos)
		{
			//find the compressed data via internal_pos, and then
			auto compressedData = region->GetChunk(internal_pos);
			//make sure the data exists, and its compressed.
			if (get<1>(compressedData) != 0 && get<1>(compressedData) < Chunk::chunkSize)
			{
				auto decompressedData = decompress(get<0>(compressedData), get<1>(compressedData), Chunk::chunkSize);
				return get<0>(decompressedData);
			}
			//ok, so maybe its not compressed, make sure its only the size of the chunk, if it is, just return the 'compressed' (raw) version.
			else if (get<1>(compressedData) == Chunk::chunkSize)
			{
				return shared_ptr<BlockID>(get<0>(compressedData));
			}
		}
		//well, we didn't find a damn thing, so default you get nothing
		return nullptr;
	}
}
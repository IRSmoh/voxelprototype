#include "VoxelLib.h"
namespace Voxel
{
	VoxelMesh::VoxelMesh()
	{
		dataPassCount = 0;
	}
	VoxelMesh::~VoxelMesh()
	{

	}
	void VoxelMesh::clear()
	{
		vertices.clear();
		vertices.shrink_to_fit();
		normals.clear();
		normals.shrink_to_fit();
		uvs.clear();
		uvs.shrink_to_fit();
		colors.clear();
		colors.shrink_to_fit();
		indices.clear();
		indices.shrink_to_fit();
		dataPassCount = 0;
	}
	void VoxelMesh::addData(VoxelModelData& data)
	{
		unsigned int i;
		if (data.vertices != NULL)
		{
			for (i = 0; i < VoxelModelData::vertCount; i++)
			{
				vertices.push_back(data.vertices[i]);
			}
		}
		if (data.normals != NULL)
		{
			for (i = 0; i < VoxelModelData::normalCount; i++)
			{
				normals.push_back(data.normals[i]);
			}
		}
		if (data.uvs != NULL)
		{
			for (i = 0; i < VoxelModelData::uvCount; i++)
			{
				uvs.push_back(data.uvs[i]);
			}
		}
		if (data.colors != NULL)
		{
			for (i = 0; i < VoxelModelData::colorCount; i++)
			{
				colors.push_back(data.colors[i]);
			}
		}
		if (data.indices != NULL)
		{
			for (i = 0; i < VoxelModelData::indexCount; i++)
			{
				indices.push_back(data.indices[i] + (dataPassCount * 4));
			}
		}
		dataPassCount++;
	}
	void VoxelMesh::pushMesh(std::shared_ptr<Model> model)
	{
		vertices.shrink_to_fit();
		model->setGeometry(vertices, (unsigned int)vertices.size());

		normals.shrink_to_fit();
		model->setNormal(normals, (unsigned int)normals.size());

		colors.shrink_to_fit();
		model->setVertexColors(colors, (unsigned int)colors.size());

		uvs.shrink_to_fit();
		model->setTextureCoordinates(uvs, (unsigned int)uvs.size());

		indices.shrink_to_fit();
		model->setIndexBuffer(indices, (unsigned int)indices.size());
		clear();
	}
}
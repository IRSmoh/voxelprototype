# Spawn Engine #

v0.0.0.000001 (absurdly alpha)

current build: [release](https://drive.google.com/file/d/0BxC4nSzRWWpJNGtMZDlJeU82VEU/view?usp=sharing)

![asd](http://i.imgur.com/tvhOKiOl.jpg)
![asd](http://i.imgur.com/NmaKb7ul.jpg)

### Tech ###
* Voxel Engine written in C++11
* Rendered with GLFW3.0 + Opengl 4.X (eventually Vulkan)
* Object oriented back-end for voxels
* Component-System based architecture for vast majority of project (with a focus on later easy scripting)

## Getting Started: ##
###Windows###
* Install Visual Studio 2013
* download/clone project
* load project into Visual Studio
* Change project settings to Release, and build for x64
* wait for project to finish building
* run

###To give out a runnable executable:###
* After building go to $project_root/x64/Release
* Delete Voxelprototype.pdb (the debug database)
* zip the Release folder, pass around, run, enjoy.
###Linux & Mac###
Not yet supported

#Dependencies:#
For a standard dev machine (one with recent AMD or Nvidia drivers), nothing extra is required beyond the files in the repo already.

###Full Dependencies:###
* GLFW 3.X
* Glew 12.X
* Bullet
* A C++11 compiler

##Tested Against:##
###System Specs:###
* OS: Windows 7 Ultimate SP1
* CPU: i7 4770k 3.5ghz
* GPU: Nvidia GTX 770 (Asus ver)
* RAM: 16GB 1600 9-9-9-24
* SSD: Samsung 840 Evo